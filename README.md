SubCIC : Ordered syntactic models for CIC
====================================================


This repository contains the Coq developments of prototypes for syntactic models
of CIC where types are interpreted as naturally ordered mathematical objects.

Theres are currently 3 prototypes that are not entirely orthogonal:
- A model based on types equipped with predicates, reminiescent of refinement types
- A variation of the first model admiting function types that are contravariant
in their domain thanks to partial functions.
- A model based on preorders inspired by the setoid model


Thanks to [alectryon](https://github.com/cpitclaudel/alectryon), you can explore the development directly in your browser:
- [The subset model](https://kmaillar.gitlabpages.inria.fr/subcic/SubsetSProp.html)
- [The preorder model](https://kmaillar.gitlabpages.inria.fr/subcic/PreorderSProp.html) and its components ([dependent product](https://kmaillar.gitlabpages.inria.fr/subcic/DependentProduct.html), [universe](https://kmaillar.gitlabpages.inria.fr/subcic/UnivIndInd.html))



Requirements
--------------

- Coq >= 8.12
- The [Equations](https://github.com/mattam82/Coq-Equations) library
- The [sprop-tools](https://github.com/kyoDralliam/SPropTools) library
- The universe hierarchy of the preorder model requires inductive-inductive types
  as implemented in the following patched version of Coq
  https://github.com/mattam82/coq/tree/inductive-inductive-types


Compilation
-------------

From the top directory, run
```shell
$ make
```

Organisation of the files:
-------------------------------

- theories/Preamble.v: pervasives definitions and tactics

- theories/Subset/ : subset model

- theories/Preorder/ : preorder model
