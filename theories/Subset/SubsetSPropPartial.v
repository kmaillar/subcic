From Coq Require Program.
From Coq Require Import ssreflect.
From SubCIC Require Import Preamble.
From SubCIC.Subset Require Import PartialDependentProduct.
From SubCIC.Subset Require Import SubsetSProp.
From SPropTools Require Logic.

Import Logic.SPropNotations.

Set Universe Polymorphism.
Set Primitive Projections.


(*|
## Non-dependent function types
|*)

Definition arrP (A B : El type) (f : wit A ⇀ wit B) :=
  sΣ incl: (forall a: wit A, wit A a -> pArrow_dom f a),
           forall (a: wit A) (w: wit A a), wit B (f a (incl a w)).

Definition arr (A B : El type) : El type :=
  mkType (mkTy (wit A ⇀ wit B) (arrP A B)).

Program Definition app_arr {A B : El type} (f : El (arr A B)) : El A -> El B :=
  fun a => mkSubset (wit B) (wit f (wit a) _) _.
Next Obligation.
  move=> A B f a.
  move: a => [a pf_a].
  move: f => [[dom_f f] //= pf_f] //=.
  by apply pf_f.
Defined.
Next Obligation.
  move=> A B f a.
  move: a => [a pf_a] //=.
  move: f => [[dom_f f] //= pf_f] //=.
  move: pf_f => [incl pf_fa].
  done.
Qed.

Program Definition lam_arr {A B : El type} (f : El A -> El B) : El (arr A B) :=
  mkSubset (pred (wit (arr A B)))
           {| pArrow_dom := wit A
            ; pArrow_app a pf := wit (f (mkSubset (wit A) a pf))
           |} _.
Next Obligation.
  move=> A B f.
  unshelve econstructor. by trivial.
  move=> a w //=. set X := f _. move: X => [fa pf_fa] //=.
Qed.

(*| The proof of the following lemma shows that the conversion rule beta
    is preserved on the nose through the subset translation. |*)
Lemma beta_valid {A B : El type} : forall (f: El A -> El B), app_arr (lam_arr f) = f.
  done.
Qed.

(*|
### Interpretation of contexts
|*)


Definition sigma0 (A : El type) (B : El (arr A type)) : El type :=
  mkType (mkTy {a : El A & wit (app_arr B a)} (fun '(existT _ a b) => wit (app_arr B a) b)).

Definition type_in (Γ : ctx) := El (arr Γ type).
Definition snoc (Γ : ctx) (A : type_in Γ) : ctx :=
  sigma0 Γ A.

Definition const {A B : El type} (b : El B) : El (arr A B) :=
  lam_arr (fun _ => b).


Definition pi0C (A : El type) (B : El (arr A type)) :=
  (* TODO: comment this *)
  pPi (map_pArrow (fun x => carrier (wit x)) (slam_pArrow (app_arr B))).

Definition pi0P (A : El type) (B : El (arr A type)) (f : pi0C A B) :=
  sΣ incl: (forall a: wit A, wit A a -> pPi_dom f a),
           forall (a: wit A) (w: wit A a), wit (app_arr B (mkSubset (wit A) a w)) (f a (incl a w)).

Definition pi0 (A : El type) (B : El (arr A type)) : El type :=
  mkType (mkTy (pi0C A B) (pi0P A B)).


Definition pi {Γ : ctx} (A : type_in Γ) (B : type_in (snoc Γ A)) : type_in Γ.
Proof.
  apply lam_arr; intros γ; apply mkType.
  apply (pi0 (app_arr A γ)).
  apply lam_arr; intros a.
  apply (app_arr B).
  simple refine (mkSubset _ _ _).
  exact (existT _ γ (wit a)).
  exact (pf a).
Defined.


Definition term (Γ : ctx) (A : type_in Γ) := El (pi0 Γ A).


From Equations Require Import Equations.

(*|
We derive rules on this "semantic" subtyping judgements.
These rules should inform the design of the syntactic counterpart
on the source type theory extending CIC to be defined.
|*)

Lemma arr_subtype {A A' B B' : El type} (hA : A' ⊆ A) (hB : B ⊆ B') :
  arr A B ⊆ arr A' B'.
Proof.
  move: A A' B B' hA hB=> [[A pA] ?] [[A' pA'] ?] [[B pB] ?] [[B' pB'] ?] [eA implA] [eB implB].
  unfold same_carrier,arr in *; simpl in *.
  unshelve econstructor.
  - red. simpl. rewrite eA eB //.
  - move=> /= f [/= hfdom hf]. unfold arrP=> /=.
    depelim eA; depelim eB.
    unshelve econstructor.
    + move=> a ha; apply hfdom. apply: implA=> //.
    + move => a w; apply: implB; apply: hf.
Qed.
