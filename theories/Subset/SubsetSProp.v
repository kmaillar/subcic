From Coq Require Program.
From Coq Require Import ssreflect.
From SubCIC Require Import Preamble.
From SPropTools Require Logic.

Import Logic.SPropNotations.

Set Universe Polymorphism.
Set Primitive Projections.


(*|
Subset types
============

A type is interpreted as a pair of a carrier and a proof-irrelevant predicate on the carrier:
|*)

Record ty := mkTy { carrier :> Type ; pred :> carrier -> SProp }.

(*|
Simple examples of subset types
-------------------------------
|*)

Definition nat_t := mkTy nat (fun _ => Squash True).
Definition pos_t := mkTy nat (fun n => Squash (n > 0)).
Definition type_t := mkTy ty (fun _ => Squash True).

(*|
Given a type A, it's decoding is given by El A
|*)

Definition El (A : subset ty (fun _ => Squash True)) := subset (wit A) (wit A). (* Wah... *)

Definition type := mkSubset (fun _ => Squash True) type_t (squash I).
Definition mkType (A : ty) : El type := mkSubset (fun _ => Squash True) A (squash I).

Check El : El type -> Type.
Check type : El type.
Check eq_refl : type = (mkType type_t).

Definition nat_ := mkType nat_t.


(*|
Non-dependent function types
----------------------------
|*)

Definition arrP (A B : El type) (f : El A -> wit B) :=
  forall (a: El A), wit B (f a).

Definition arr (A B : El type) : El type :=
  mkType (mkTy (El A -> wit B) (arrP A B)).

Program Definition app_arr {A B : El type} (f : El (arr A B)) : El A -> El B :=
  fun a => mkSubset (wit B) (wit f a) _.
Next Obligation.
  move=> A B f a.
  move: f => [f pf_f] //=.
Qed.

Program Definition lam_arr {A B : El type} (f : El A -> El B) : El (arr A B) :=
  mkSubset (pred (wit (arr A B))) (fun a => wit (f a)) _.
Next Obligation.
  move=> A B f a.
  move: (f a) => [b pf_b] //=.
Qed.

(*| The proof of the following lemma shows that the conversion rule beta
    is preserved on the nose through the subset translation. |*)
Lemma beta_valid {A B : El type} : forall (f: El A -> El B), app_arr (lam_arr f) = f.
  done.
Qed.

(*|
Interpretation of contexts
--------------------------
|*)


Definition empty : El type :=
  mkType (mkTy False (fun _ => Squash True)).

Definition sigma0 (A : El type) (B : El (arr A type)) : El type :=
  mkType (mkTy {a : El A & wit (app_arr B a)} (fun '(existT _ a b) => wit (app_arr B a) b)).

Definition ctx := El type.
Definition nil : ctx := empty.
Definition type_in (Γ : ctx) := El (arr Γ type).
Definition snoc (Γ : ctx) (A : type_in Γ) : ctx :=
  sigma0 Γ A.

Definition const {A B : El type} (b : El B) : El (arr A B) :=
  lam_arr (fun _ => b).


Definition pi0C (A : El type) (B : El (arr A type)) :=
  forall (a: El A), wit (app_arr B a).

Definition pi0P (A : El type) (B : El (arr A type)) (f : pi0C A B) :=
  forall (a: El A), wit (app_arr B a) (f a).

Definition pi0 (A : El type) (B : El (arr A type)) : El type :=
  mkType (mkTy (pi0C A B) (pi0P A B)).


Definition pi {Γ : ctx} (A : type_in Γ) (B : type_in (snoc Γ A)) : type_in Γ.
Proof.
  apply lam_arr; intros γ; apply mkType.
  apply (pi0 (app_arr A γ)).
  apply lam_arr; intros a.
  apply (app_arr B).
  simple refine (mkSubset _ _ _).
  exact (existT _ γ (wit a)).
  exact (pf a).
Defined.


Definition term (Γ : ctx) (A : type_in Γ) := El (pi0 Γ A).


(*|
Interpretation of the subtyping judgement
-----------------------------------------
|*)

Import EqNotations.

Definition same_carrier (A A' : El type) :=
  carrier (wit A) = carrier (wit A').

Definition rew_carrier {A A' : El type} (h : same_carrier A A') (a : wit A) : wit A'.
Proof. by induction h. Defined.

Definition subtype (A A' : El type) :=
  { h : same_carrier A A' ≫ forall (a : wit A), wit A a -> wit A' (rew_carrier h a) }.

Notation "X ⊆ Y" := (subtype X Y) (at level 70). (* random level *)

