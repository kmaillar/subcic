From Coq Require Program.
From Coq Require Import StrictProp.
From SubCIC Require Import Preamble.

Set Implicit Arguments.
Set Universe Polymorphism.
Set Primitive Projections.

Record pArrow (A: Type) (B: Type) :=
  { pArrow_dom: A -> SProp
  ; pArrow_app:> forall (x:A), pArrow_dom x -> B
  }.

Definition lam_pArrow {A B: Type} (f: A -> B) : pArrow A B :=
  {| pArrow_dom _ := Squash True
   ; pArrow_app x _ := f x
  |}.

Definition slam_pArrow {A B: Type} {P: A -> SProp} (f: subset A P -> B) : pArrow A B :=
  {| pArrow_dom x := P x
   ; pArrow_app x pf := f (mkSubset P x pf)
  |}.

Record pPi (A: Type) (B: pArrow A Type) :=
  { pPi_dom: A -> SProp
  ; pPi_incl: forall (x: A), pPi_dom x -> pArrow_dom B x
  ; pPi_app:> forall (x:A) (wit: pPi_dom x), B x (pPi_incl wit)
  }.

Program Definition lam_pPi {A: Type} {B: A -> Type} (f: forall (x: A), B x) : pPi (lam_pArrow B) :=
  {| pPi_dom _ := Squash True
   ; pPi_app x _ := f x
  |}.
Next Obligation.
  by simpl.
Qed.

Program Definition pArrow2pPi {A B: Type} (f: pArrow A B) : pPi (lam_pArrow (fun (_: A) => B)) :=
  {| pPi_dom := pArrow_dom f
   ; pPi_app x wit := f x wit
  |}.
Next Obligation.
  by simpl.
Qed.

Definition pPi2pArrow {A B: Type} (f: pPi (lam_pArrow (fun (_: A) => B))) : pArrow A B :=
  {| pArrow_dom := pPi_dom f
   ; pArrow_app x wit := f x wit
  |}.

Lemma l1 {A B: Type} (f: pArrow A B) : pPi2pArrow (pArrow2pPi f) = f.
  done.
Qed.

Lemma l2 {A B: Type} (f: pPi (lam_pArrow (fun (_: A) => B))) : pArrow2pPi (pPi2pArrow f) = f.
  done.
Qed.

Coercion pArrow2pPi_coerce := @pArrow2pPi.
(* Coercion pPi2pArrow_coerce := @pPi2pArrow. *)

Notation "x ⇀ y" := (pArrow x y)
  (at level 99, y at level 200, right associativity): type_scope.

(* TODO: better name? *)
(* comp_pArrow ? *)
Definition map_pArrow {A B C: Type} (g: B -> C) (f: A ⇀ B) : A ⇀ C :=
  {| pArrow_dom := pArrow_dom f
   ; pArrow_app x wit := g (pArrow_app f x wit)
  |}.

