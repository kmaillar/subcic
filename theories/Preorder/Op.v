From SubCIC Require Import preamble preorderSProp dependentProduct preorderUnivIndInd.

Set Primitive Projections.
Set Universe Polymorphism.

Import PENotations.


Definition op_discr_iso A : preorder_iso (op (discr A)) (discr A).
Proof.
  ueconstr.
  1,2: ueconstr; first (move=> x; exact x);
  sabstract (move=> ? ? h; induction h; apply: reflexive).
  all: PE.refl.
Defined.

Definition id_iso (A : Preorder) : preorder_iso A A.
Proof.
  ueconstr; try apply: idmon; PE.refl.
Defined.

Definition opvar (v : variance) : variance :=
  match v with
  | Cov => Contra
  | Contra => Cov
  | Biv => Biv
  end.

Lemma vrel_opvar [A : Preorder] :
  forall v (a0 a1: A), vrel v a0 a1 -> vrel (opvar v) a1 a0.
Proof.
  move=> [] ? ? //=.
  move=> ?; by apply: equiv_sym.
Qed.

Lemma vrel_opvar_inv [A : Preorder] :
  forall v (a0 a1: A), vrel (opvar v) a0 a1 -> vrel v a1 a0.
Proof.
  move=> [] ? ? //=.
  move=> ?; by apply: equiv_sym.
Qed.

Lemma vrel_mon [A0 A1 : Preorder] (f : A0 →mon A1) :
  forall v (a0 a1 : A0), vrel v a0 a1 -> vrel v (f a0) (f a1).
Proof.
  move=> [] ?? /=; try apply: monotone.
  move=> [? ?]; split; by apply: monotone.
Qed.

Lemma vrel_left_equiv [A : Preorder] :
  forall v (x y y' : A), equiv y y' -> vrel v x y ->  vrel v x y'.
Proof.
  move=> [] ? ? ?; last (move=> ??; apply: equiv_trans; eassumption).
  all: move=> [? ?] ?; apply: transitive; eassumption.
Qed.

Definition comp_op_mon [A0 A1 A2] (g : A0 →mon A1) (f : A1 →mon A2)
  : op_mon f ∘ op_mon g ≈ op_mon (f ∘ g).
Proof. PE.refl. Qed.

Definition op_mon_monotone [A0 A1] (f g : A0 →mon A1) :
  f ≈ g -> op_mon f ≈ op_mon g.
Proof. move=> fg x /=; move: (fg x) => [? ?]; split=> //. Qed.



Section OpUniv.
  Context {U: Preorder}.

  Definition opUnivP (Ac : code (U:=U)) := code (U:=U).

  Definition opUnivQ
             Ac0 (opAc0: opUnivP Ac0)
             Ac1 (opAc1: opUnivP Ac1)
             (rc : rcode Ac0 Ac1) :=
    rcode opAc0 opAc1.
    (* { oprc : rcode (dfst opAc0) (dfst opAc1) ≫ *)
    (*   dsnd opAc1 ∘ op_mon (Elrel rc) ≈ Elrel oprc ∘ dsnd opAc0 }. *)

  Definition opUnivR A Ac (opAc : opUnivP Ac) (hA : el A Ac) :=
    preorder_iso (op A) (El opAc).

  Definition opUnivS
             A0 Ac0 (opAc0 : opUnivP Ac0) hA0 (hopA0 : opUnivR A0 Ac0 opAc0 hA0)
             A1 Ac1 (opAc1 : opUnivP Ac1) hA1 (hopA1 : opUnivR A1 Ac1 opAc1 hA1)
             (f : A0 →mon A1) (rc : rcode Ac0 Ac1)
             (oprc : opUnivQ _ opAc0 _ opAc1 rc)
             (_ : elrel hA0 hA1 f rc) :=
    Box (hopA1 ∘ op_mon f ≈ Elrel oprc ∘ hopA0).
    (* elrel hopA0 hopA1 (op_mon f) oprc. *)

  Definition op_univ_piDataC
             (v : variance)
             (pc : PiData.c)
             (ec : @PiData.ec _ _ _ _ opUnivP opUnivQ opUnivR opUnivS pc)
    : @PiData.c (code (U:=U)) rcode el elrel.
  Proof.
    ueconstr.
    + exact (PiData.Ac pc).
    + exact (PiData.A pc).
    + ueconstr.
      * exact (fun a => El (PiData.ihBc ec a)).
      * move=> a0 a1 a01 /=. exact (Elrel (PiData.ihBcmon ec a0 a1 a01)).
      * move=> ? ? ? xy yz /=.
        rhs (apply: Elrel_rcode_trans). apply: Elrel_irr.
      * move=> a /=.
        rhs (apply: Elrel_rcode_refl).
        apply: Elrel_irr.
    + exact (PiData.ihBc ec).
    + move=> a /=. apply: El_valid.
    + move=> a0 a1 a01; exact (PiData.ihBcmon ec a0 a1 a01).
    + exact (PiData.hA pc).
    + move=> a0 a1 a01 /=.
      apply: Elrel_valid.
  Defined.

  Definition op_univ_piDataRc
             (v : variance)
             (c0 : PiData.c)
             (ec0 : PiData.ec c0)
             (c1 : PiData.c)
             (ec1 : PiData.ec c1)
             (rc : PiData.rc c0 c1)
             (erc : PiData.erc ec0 ec1 rc) :
    PiData.rc (op_univ_piDataC v c0 ec0) (op_univ_piDataC v c1 ec1).
  Proof.
    ueconstr.
    + exact (PiData.A01 rc).
    + exact (PiData.Ac01 rc).
    + ueconstr.
      * move=> a /=. apply: (Elrel (PiData.ihBc01 erc a)).
      * move=> a0 a1 a01 /=.
        lhsrev (apply: Elrel_rcode_trans).
        rhs (apply: Elrel_rcode_trans).
        apply: Elrel_irr.
    + move=> a1. exact (PiData.ihBc01 erc a1).
    + exact (PiData.hA01 rc).
    + move=> a1 /=. apply: Elrel_valid.
  Defined.

  Definition op_univ_aux :
    @elim_all_code_res U opUnivP opUnivQ opUnivR opUnivS.
  Proof.
    uref (@elim_all_code U).
    (* opUnivP *)
    - exact Cnat.
    - move=> v pc ec; apply: (Cpi (opvar v)) ; apply: op_univ_piDataC; eassumption.
    - exact CuOp.
    - exact Cu.

    (* opUnivQ *)
    - constructor.
    - move=> v c0 ec0 c1 ec1 rc erc.
      constructor; apply: op_univ_piDataRc; eassumption.
    - constructor.
    - constructor.

    (* opUnivR *)
    - apply: op_discr_iso.
    - move=> v c ec /=.
      ueconstr.
      + ueconstr.
        * move=> f; ueconstr; first exact (fun a => PiData.ihB ec a (f a)).
          sabstract (
          move=> x y xy /=; destruct (PiData.ihBmon ec x y xy) as [h]; apply: vrel_opvar;
          apply: vrel_left_equiv; first apply: h;
          apply: vrel_mon; apply: vp_mon).
        * move=> f g fg /= a; apply: monotone; exact (fg a).
      + ueconstr.
        * move=> f; ueconstr; first exact (fun a => bwd (PiData.ihB ec a) (f a)).
          sabstract (
            move=> x y xy /=; destruct (PiData.ihBmon ec x y xy) as [h]; apply: vrel_opvar_inv;
            apply pointwise_equiv_sym in h;
            apply: vrel_left_equiv;
            first (apply: equiv_sym; apply: (iso_flip_eq _ _ _ _ h));
            apply: (vrel_mon (bwd (PiData.ihB ec y)) (opvar v));
            apply: vp_mon ).
        * move=> f g fg /= a; refine (monotone (bwd (PiData.ihB ec a)) _) ; exact (fg a).
      + move=> f /=; split=> /= a;
                          [refine (sprl (fbeq (PiData.ihB ec a) _))
                          | refine (sprr (fbeq (PiData.ihB ec a) _))].
      + move=> f /=; split=> /= a;
                          [refine (sprl (bfeq (PiData.ihB ec a) _))
                          | refine (sprr (bfeq (PiData.ihB ec a) _))].
   - apply: id_iso.
   - apply: id_iso.


   (* opUnivS *)
   - constructor; PE.refl.
   - move=> v c0 ec0 c1 ec1 rc erc; constructor.
     simpl.
     move=> f /=; split=> a /=;
     destruct (PiData.ihB01 erc a) as [h];
                        [refine (sprl (h _)) | refine (sprr (h _))].
   - constructor; PE.refl.
   - constructor; PE.refl.
  Defined.

  Definition Op : Univ (U:=U) →mon Univ (U:=U).
  Proof.
    exists (ecode op_univ_aux).
    move=> c0 c1 sq; constructor.
    apply: (ercode op_univ_aux (recov_rcode sq)).
  Defined.

End OpUniv.


