From SubCIC Require Import preamble preorderSProp dependentProduct preorderUnivSpec.

Set Universe Polymorphism.

Import PENotations.

(* This section parametrize the whole file *)
Section UnivParam.
  Context {U : Preorder}.

Section CodePackImpl.

  Unset Universe Checking.
  Monomorphic Universe c.

  Inductive code : Type@{c} :=
  | Cnat : code
  | Cpi (v : variance) (c : @PiData.c code rcode el elrel)
    : code
  | Cu : code
  | CuOp : code

  with rcode : code -> code -> Type@{c} :=
  | RCnat : rcode Cnat Cnat
  | RCpi v [c0 c1 : PiData.c] (rc : PiData.rc c0 c1)
    : rcode (Cpi v c0) (Cpi v c1)
  | RCu : rcode Cu Cu
  | RCuOp : rcode CuOp CuOp


  with el : Preorder@{c} -> code -> Type@{c} :=
  | Elnat : el (discr nat) Cnat
  | Elpi v (c : PiData.c) : el (Πvar v (PiData.A c) (PiData.B c)) (Cpi v c)
  | Elu : el U Cu
  | EluOp : el (op U) CuOp

  with elrel : forall [A0 Ac0] (hA0 : el A0 Ac0) [A1 Ac1] (hA1 : el A1 Ac1),
      (A0 →mon A1) -> rcode Ac0 Ac1 -> Type@{c} :=
  | ElRnat : elrel Elnat Elnat (idmon _) RCnat
  | ElRpi v [c0 c1 : PiData.c] (rc : PiData.rc c0 c1)
    : elrel (Elpi v c0) (Elpi v c1) (Πvar_map v (PiData.A01 rc) (PiData.B01 rc)) (RCpi v rc)
  | ElRu : elrel Elu Elu (idmon _) RCu
  | ElRuOp : elrel EluOp EluOp (idmon _) RCuOp
  .

  Set Universe Checking.

  Definition cp := mkCodePack code rcode el elrel.

  Section ElimCode.
    Context {P : elimPTy (cp :=cp)} {Q : elimQTy P} {R : elimRTy P} {S : elimSTy P Q R}.
    Arguments Q [_] _ [_] _ _.
    Arguments R [_ _] _ _.
    Arguments S [_ _ _ _] _ [_ _ _ _] _ [_ _] _ _.

    Notation ec := (@PiData.ec code rcode el elrel P Q R S).
    Notation erc := (fun ec0 => @PiData.erc code rcode el elrel P Q R S _ ec0 _).

    Context (cnat : P Cnat)
            (cpi : forall v c, ec c -> P (Cpi v c))
            (cu : P Cu)
            (cuop : P CuOp).
    Arguments cpi _ [_] _.

    Context (rcnat : Q cnat cnat RCnat)
            (rcpi : forall v c0 (ec0 : ec c0) c1 (ec1 : ec c1) rc,
                erc ec0 ec1 rc -> Q (cpi v ec0) (cpi v ec1) (RCpi v rc))
            (rcu : Q cu cu RCu)
            (rcuop : Q cuop cuop RCuOp).
    Arguments rcpi _ [_ _ _ _ _] _.

    Context (elnat : R cnat Elnat)
            (elpi : forall v c (ec : ec c), R (cpi v ec) (Elpi v c))
            (elu : R cu Elu)
            (eluop : R cuop EluOp).
    Arguments elpi _ [_] _.

    Context (elrelnat : S elnat elnat rcnat ElRnat)
            (elrelpi : forall v c0 (ec0 : ec c0) c1 (ec1 : ec c1)
                        rc (erc : erc ec0 ec1 rc),
                S (elpi v ec0) (elpi v ec1) (rcpi v erc) (ElRpi v rc))
            (elrelu : S elu elu rcu ElRu)
            (elreluop : S eluop eluop rcuop ElRuOp).
    Arguments elrelpi _ [_ _ _ _ _] _.

    Definition elim_code : forall (c : code), P c.
    Proof.
    unshelve refine (
      fix elim_code (c : code) {struct c} : P c :=
        match c with
        | Cnat => _
        | Cpi v pc => _
        | Cu => _
        | CuOp => _
        end
      with elim_rcode [c0 c1] (rc : rcode c0 c1) {struct rc} : Q (elim_code c0) (elim_code c1) rc :=
        match rc with
        | RCnat => _
        | RCpi v prc => _
        | RCu => _
        | RCuOp => _
        end
      with elim_el [A Ac] (hAc : el A Ac) {struct hAc} : R (elim_code Ac) hAc :=
        match hAc with
        | Elnat => _
        | Elpi v prc => _
        | Elu => _
        | EluOp => _
        end
      with elim_elrel [A0 Ac0] [hA0 : el A0 Ac0]
          [A1 Ac1]  [hA1 : el A1 Ac1] [f rc]
          (hrc : elrel hA0 hA1 f rc) {struct hrc} : S (elim_el hA0) (elim_el hA1) (elim_rcode rc) hrc :=
        match hrc with
        | ElRnat => _
        | ElRpi v prc => _
        | ElRu => _
        | ElRuOp => _
        end
      for elim_code
      ); try (simpl ; assumption).
    simpl; uref cpi; ueconstr; intros=> /=; eauto.
    simpl; uref rcpi; ueconstr; intros=> /=; eauto.
    simpl; uref elpi; ueconstr; intros=> /=; eauto.
    simpl; uref elrelpi; ueconstr; intros=> /=; eauto.
    Defined.


    Definition elim_rcode : forall [c0 c1] (rc : rcode c0 c1), Q (elim_code c0) (elim_code c1) rc.
    Proof.
    unshelve refine (
      fix elim_code (c : code) {struct c} : P c := _
      with elim_rcode [c0 c1] (rc : rcode c0 c1) {struct rc} : Q (elim_code c0) (elim_code c1) rc :=
          _
      with elim_el [A Ac] (hAc : el A Ac) {struct hAc} : R (elim_code Ac) hAc := _
      with elim_elrel [A0 Ac0] [hA0 : el A0 Ac0]
          [A1 Ac1]  [hA1 : el A1 Ac1] [f rc]
          (hrc : elrel hA0 hA1 f rc) {struct hrc} : S (elim_el hA0) (elim_el hA1) (elim_rcode rc) hrc := _
      for elim_rcode
            ).
    Defined.

    Definition elim_el : forall [A Ac] (hAc : el A Ac), R (elim_code Ac) hAc.
    Proof.
    unshelve refine (
      fix elim_code (c : code) {struct c} : P c := _
      with elim_rcode [c0 c1] (rc : rcode c0 c1) {struct rc} : Q (elim_code c0) (elim_code c1) rc :=
          _
      with elim_el [A Ac] (hAc : el A Ac) {struct hAc} : R (elim_code Ac) hAc := _
      with elim_elrel [A0 Ac0] [hA0 : el A0 Ac0]
          [A1 Ac1]  [hA1 : el A1 Ac1] [f rc]
          (hrc : elrel hA0 hA1 f rc) {struct hrc} : S (elim_el hA0) (elim_el hA1) (elim_rcode rc) hrc := _
      for elim_el
            ).
    Defined.

    Definition elim_elrel : forall [A0 Ac0] [hA0 : el A0 Ac0]
              [A1 Ac1]  [hA1 : el A1 Ac1] [f rc]
              (hrc : elrel hA0 hA1 f rc), S (elim_el hA0) (elim_el hA1) (elim_rcode rc) hrc.
    Proof.
    unshelve refine (
      fix elim_code (c : code) {struct c} : P c := _
      with elim_rcode [c0 c1] (rc : rcode c0 c1) {struct rc} : Q (elim_code c0) (elim_code c1) rc :=
          _
      with elim_el [A Ac] (hAc : el A Ac) {struct hAc} : R (elim_code Ac) hAc := _
      with elim_elrel [A0 Ac0] [hA0 : el A0 Ac0]
          [A1 Ac1]  [hA1 : el A1 Ac1] [f rc]
          (hrc : elrel hA0 hA1 f rc) {struct hrc} : S (elim_el hA0) (elim_el hA1) (elim_rcode rc) hrc := _
      for elim_elrel
            ).
    Defined.

    Record elim_all_code_res :=
      { ecode : forall c, P c
      ; ercode : forall [c0 c1] (rc : rcode c0 c1), Q (ecode c0) (ecode c1) rc
      ; eel : forall [A Ac] (hAc : el A Ac), R (ecode Ac) hAc
      ; eelrel : forall [A0 Ac0] [hA0 : el A0 Ac0]
              [A1 Ac1]  [hA1 : el A1 Ac1] [f rc]
              (hrc : elrel hA0 hA1 f rc), S (eel hA0) (eel hA1) (ercode rc) hrc
      }.

    Definition elim_all_code : elim_all_code_res.
    Proof. ueconstr; [exact elim_code | exact elim_rcode | exact elim_el | exact elim_elrel ]. Defined.

  End ElimCode.

  Arguments elim_all_code : clear implicits.

End CodePackImpl.



Section ElDefinition.


  Definition elDefP : elimPTy (cp:=cp) :=
    fun (Ac : code) => { A : Preorder ⫳ el A Ac }.

  Definition elDefQ : elimQTy elDefP :=
    fun _ ihc0 _ ihc1 rc01 =>
      { f : dfst ihc0 →mon dfst ihc1
        ⫳ elrel (dsnd ihc0) (dsnd ihc1) f rc01 }.

  Definition elDefR : elimRTy elDefP :=
    fun A Ac ihAc hA =>
      (* Is there any relation between hA and dsnd ihAc ? *)
      preorder_iso A (dfst ihAc).

  Definition elDefS : elimSTy elDefP elDefQ elDefR :=
    fun _ _ ihAc0 _ ihA0 _ _ ihAc1 _ ihA1 A01 _ ihAc01 _ =>
      (* Further relations to add ? *)
      Box (ihA1 ∘ A01 ≈ dfst ihAc01 ∘ ihA0).

  Definition ElDef :
    @elim_all_code_res elDefP elDefQ elDefR elDefS.
  Proof.
    uref @elim_all_code.
    (* El *)
    + exists (discr nat); constructor.
    + move=> v pc ?; exists (Πvar v (PiData.A pc) (PiData.B pc)); constructor.
    + exists U; constructor.
    + exists (op U); constructor.


    (* Elrel *)
    + (* exact (idmon _). (* fills the hole with El Cnat; complains that the recursive call is ill-formed *) *)
      exists (idmon (discr nat)); constructor.
    + move=> v ? ? ? ? prc ?;
      exists (Πvar_map v (PiData.A01 prc) (PiData.B01 prc));
      constructor.
    + simpl ; exists (idmon _) ; constructor.
    + simpl ; exists (idmon _) ; constructor.

    (* El_equiv *)
    + ueconstr; try exact (idmon _) ; uref pointwise_equiv_refl.
    + move=> /= *; ueconstr ; try uref Πvar_map.
      1,3: apply: idmon.
      1,2: apply: natTrans_id_skew.
      all: move=> f; split=> x /=; apply: reflexive.
    + ueconstr; try exact (idmon _) ; uref pointwise_equiv_refl.
    + ueconstr; try exact (idmon _) ; uref pointwise_equiv_refl.

    (* Elrel_natural*)
    + constructor; PE.refl.
    + move=> /= *; constructor.
      lhs apply: Πvar_map_comp.
      rhsrev apply: Πvar_map_comp.
      uref Πvar_map_ext; first PE.refl.
      move=> ? /=.
      uref comp_monotone_right_id_ext; first PE.refl.
      uref iid.
    + simpl; constructor; PE.refl.
    + simpl; constructor; PE.refl.
  Defined.

End ElDefinition.

Notation norm_type u := (ltac:(let t := type of u in
                         let t' := eval hnf in t in exact t')).

Definition El (c : code): Preorder := dfst (ecode ElDef c).
Definition El_valid (c : code): el (El c) c := dsnd (ecode ElDef c).
Definition Elrel [c0 c1] (rc : rcode c0 c1)
  : El c0 →mon El c1
  := dfst (ercode ElDef rc).
Definition Elrel_valid [c0 c1] (rc : rcode c0 c1)
  : elrel (El_valid c0) (El_valid c1) (Elrel rc) rc
  := dsnd (ercode ElDef rc).
Definition Elrel_equiv : norm_type (eel ElDef) := Eval hnf in eel ElDef.
Arguments Elrel_equiv [_ _] _.

Definition Elrel_natural [A0 Ac0 hA0 A1 Ac1 hA1]
           [f : A0 →mon A1] [rc : rcode Ac0 Ac1]
           (hrc : elrel hA0 hA1 f rc)
  : Elrel_equiv hA1 ∘ f ≈ Elrel rc ∘ Elrel_equiv hA0.
Proof.
  exact (unbox (@eelrel _ _ _ _ ElDef A0 Ac0 hA0 A1 Ac1 hA1 f rc hrc)).
Qed.


Lemma Elrel_equiv_valid c : Elrel_equiv (El_valid c) ≈ idmon (El c).
Proof.
  induction c ; try PE.refl.
  uref Πvar_map_id; first PE.refl.
  move=> a /=; apply: comp_monotone_left_id.
  PE.sym; uref iid.
Qed.

Lemma Elrel_equiv_valid_bwd c : bwd (Elrel_equiv (El_valid c)) ≈ idmon (El c).
Proof.
  induction c ; try PE.refl.
  uref Πvar_map_id; first PE.refl.
  move=> a /=; apply: comp_monotone_left_id.
  PE.sym; uref iid.
Qed.

Definition map_to_el [A Ac] (hA : el A Ac) [B Bc] (hB : el B Bc) :
  A →mon B -> El Ac →mon El Bc :=
  fun f => fwd (Elrel_equiv hB) ∘ f ∘ bwd (Elrel_equiv hA).

Definition map_from_el [A Ac] (hA : el A Ac) [B Bc] (hB : el B Bc) :
  El Ac →mon El Bc -> A →mon B :=
  fun f => bwd (Elrel_equiv hB) ∘ f ∘ fwd (Elrel_equiv hA).

(* lots of lemmas to prove on these 2 functions... *)
(* Consider generalizing these lemmas to arbitrary isos
  when possible *)

Definition map_from_el_elrel
   [A0 Ac0] [hA0 : el A0 Ac0]
   [A1 Ac1] [hA1 : el A1 Ac1]
   [f rc]:
     elrel hA0 hA1 f rc -> map_from_el hA0 hA1 (Elrel rc) ≈ f.
Proof.
  move=> /Elrel_natural h; apply (comp_right (bwd (Elrel_equiv hA1))) in h.
  PE.sym; rhs (apply: h).
  rhsrev (apply: comp_assoc).
  apply: comp_monotone_left_id_ext; first PE.refl.
  apply: bfeq.
Qed.

Definition imap_map_from_el (pc : @PiData.c code rcode el elrel) :
  forall a0 a1 (a01 : a0 ≤ a1),
        imap (PiData.B pc) a01 ≈ map_from_el (PiData.hB pc a0)
              (PiData.hB pc a1) (Elrel (PiData.Bcmon pc a0 a1 a01)).
Proof.
  move=> a0 a1 a01; PE.sym.
  apply: (map_from_el_elrel (PiData.hBmon pc a0 a1 a01)).
Qed.

Definition comp_map_from_el
           [A0 Ac0] (hA0 : el A0 Ac0)
           [A1 Ac1] (hA1 : el A1 Ac1)
           [A2 Ac2] (hA2 : el A2 Ac2)
           f g:
      map_from_el hA1 hA2 f ∘ map_from_el hA0 hA1 g ≈
                  map_from_el hA0 hA2 (f ∘ g).
Proof.
  unfold map_from_el.
  lhs (apply: comp_assoc).
  apply: comp_monotone_split; first PE.refl.
  lhs (apply: comp_assoc).
  rhsrev (apply: comp_assoc).
  apply: comp_monotone_split; first PE.refl.
  lhsrev (apply: comp_assoc).
  PE.sym; apply: comp_monotone_right_id_ext; first PE.refl.
  apply: fbeq.
Qed.

Definition map_from_el_subst
           [A0 Ac0] (hA0 : el A0 Ac0)
           [A1 Ac1] (hA1 : el A1 Ac1)
           [f g]:
      f ≈ g ->
      map_from_el hA0 hA1 f ≈ map_from_el hA0 hA1 g.
Proof.
  move=> fg; apply: comp_monotone_split; last apply: comp_monotone_split=> //; PE.refl.
Qed.


From Coq.Program Require Import Equality.



Section Reflexivity.
  Definition reflP (c : code) := rcode c c.
  Definition reflR [A Ac] (ihAc : reflP Ac) (hA : el A Ac) :=
    { eta : A →mon A ⫳ { valid : elrel hA hA eta ihAc ≫ eta ≈ idmon _ } }.

  Definition reflP_pi (c : PiData.c) :
    @PiData.ec code rcode el elrel reflP triv_pred reflR triv_pred c ->
    PiData.rc c c.
  Proof.
    move=> [ihAc ihA ihBc ihB ihBcmon _]; ueconstr; simpl.
    + apply: (dfst ihA).
    + apply: ihAc.
    + ueconstr; first (move=> a /=; apply: (imap (PiData.B c))).
      sabstract (apply: (sprl (Spr2 (dsnd ihA) a))).
      sabstract (move=> * /=; lhsrev apply: icomp; rhs apply: icomp ; PE.refl).
    + move=> a. apply: (PiData.Bcmon c).
      apply: (sprl (Spr2 (dsnd ihA) a)).
    + apply: (Spr1 (dsnd ihA)).
    + move=> a /=; uref (PiData.hBmon c).
  Defined.

  Definition rcode_refl_aux :
    @elim_all_code_res reflP
                       triv_pred
                       reflR
                       triv_pred.
  Proof.
    uref @elim_all_code. all: try discharge_triv_pred.
    1,3,4: constructor.
    2,4,5: ueconstr; [apply: idmon | ueconstr ; [constructor| PE.refl]].

    - move=> v c ec; ueconstr; apply: reflP_pi=> //.

    - hnf. move=> v pc ec. ueconstr; last ueconstr.
      + apply: Πvar_map; exact (PiData.B01 (reflP_pi pc ec)).
      + constructor.
      + uref Πvar_map_id; first apply: (Spr2 (dsnd (PiData.ihA ec))).
        move=> ? /=; apply: comp_monotone_left_id; PE.refl.
  Defined.

End Reflexivity.

Section Transitivity.

  Definition transP (c : code) :=
    forall c0 c2, rcode c0 c -> rcode c c2 -> rcode c0 c2.

  Definition transR [A Ac] (ihAc : transP Ac) (hA : el A Ac) :=
    forall A0 Ac0 (hA0 : el A0 Ac0) A2 Ac2 (hA2 : el A2 Ac2)
      (f0 : A0 →mon A) rc0 (hrc0 : elrel hA0 hA f0 rc0)
      (f2 : A →mon A2) rc2 (hrc2 : elrel hA hA2 f2 rc2),
      elrel hA0 hA2 (f2 ∘ f0) (ihAc Ac0 Ac2 rc0 rc2).


  Definition transPnat : transP Cnat :=
    fun _ c2 rc =>
      match rc in rcode c0 c1
            return match c1 with
                   | Cnat => rcode Cnat c2 -> rcode c0 c2
                   |  _ => unit
                   end
      with | RCnat => id | _ => tt end.

  Notation ec := (@PiData.ec code rcode el elrel transP triv_pred transR triv_pred).

  Definition transPpiDataRc
    (pc0 pc1 pc2 : PiData.c)
    (prc01 : PiData.rc pc0 pc1)
    (prc12 : PiData.rc pc1 pc2)
    (ec1 : ec pc1) :
    PiData.rc pc0 pc2.
  Proof.
    ueconstr.
    * exact (PiData.A01 prc01 ∘ PiData.A01 prc12).
    * exact (PiData.ihAc ec1 _ _ (PiData.Ac01 prc12) (PiData.Ac01 prc01)).
    * apply:comp_natTrans_pb.
      exact (PiData.B01 prc01).
      exact (PiData.B01 prc12).
    * move=> a.
      apply: (PiData.ihBc ec1 (PiData.A01 prc12 a)).
      apply: (PiData.Bc01 prc01).
      apply: (PiData.Bc01 prc12).
    * apply: (PiData.ihA ec1).
      apply: (PiData.hA01 prc12).
      apply: (PiData.hA01 prc01).
    * move=> a /=.
      apply: (PiData.ihB ec1).
      apply: (PiData.hB01 prc01).
      apply: (PiData.hB01 prc12).
  Defined.



  Definition transPpi v pc (ec : ec pc) : transP (Cpi v pc).
  Proof.
    move=> c0 c2 rc; move: ec.
    refine (match rc in rcode c0 c1
                  return
                  match c1 with
                  | Cpi v pc => PiData.ec pc -> rcode (Cpi v pc) c2 -> rcode c0 c2
                  | _ => unit
                  end
            with | @RCpi v pc0 pc1 prc => _ | _ => tt end).
    clear v0 pc c0 rc.
    move=> ec1 rc ; move: prc ec1.
    refine (match rc in rcode c1 c2
                  return
                  match c1 with
                  | Cpi v pc => PiData.rc pc0 pc -> PiData.ec pc -> rcode (Cpi v pc0) c2
                  | _ => unit
                  end
            with | @RCpi v pc1 pc2 prc12 => _ | _ => tt end).
    clear c2 v0 pc3 rc.
    move=> prc01 ec1; constructor; apply: transPpiDataRc; eassumption.
  Defined.

  Definition transPu : transP Cu :=
  fun _ c2 rc =>
    match rc in rcode c0 c1
          return match c1 with
                  | Cu => rcode Cu c2 -> rcode c0 c2
                  | _ => unit
                  end
    with | RCu => id | _ => tt end.

  Definition transPuOp : transP CuOp :=
  fun _ c2 rc =>
    match rc in rcode c0 c1
          return match c1 with
                  | CuOp => rcode CuOp c2 -> rcode c0 c2
                  | _ => unit
                  end
    with | RCuOp => id | _ => tt end.


  Ltac solve_transR :=
    move=> ? ? ? ? ? ? ? ? hrc0 ? ? hrc2;
          dependent destruction hrc0;
          dependent destruction hrc2;
          constructor.


  Definition rcode_trans_aux :
    @elim_all_code_res transP
                       triv_pred
                       transR
                       triv_pred.
  Proof.
    uref @elim_all_code. all: try discharge_triv_pred.
    (* transP *)
    - apply: transPnat.
    - apply: transPpi.
    - apply: transPu.
    - apply: transPuOp.

    (* transR *)
    - solve_transR.
    - move=> v pc ec.
      hnf. move=> ? ? ? ? ? ? ? ? hrc0 ? ? hrc2.
      dependent destruction hrc0.
      dependent destruction hrc2.
      unfold transPpi.
      set rc' := (rc in RCpi v rc).
      refine (ElRpi v rc').
    - hnf; move=> A0 Ac0 hA0 A2 Ac2 hA2 f0 rc0 hrc0.
      refine (match hrc0
                    in @elrel A0 Ac0 hA0 A1 Ac1 hA1 f0 rc0
                    return
                    forall (f2 : A1 →mon A2) (rc2 :rcode Ac1 Ac2)
                      (hrc2 :elrel hA1 hA2 f2 rc2),
                    match hA1 as hA1 in el A1 Ac1
                          return
                          rcode Ac0 Ac1 ->
                          forall (f0 : A0 →mon A1) (f2 : A1 →mon A2) (rc2 :rcode Ac1 Ac2),
                          elrel hA1 hA2 f2 rc2 -> Type
                    with
                    | Elu => fun rc0 f0 f2 rc2 hrc2 =>
                      @elrel A0 Ac0 hA0 A2 Ac2 hA2 (f2 ∘ f0) (transPu Ac0 Ac2 rc0 rc2)
                    | _ => fun _ _ _ _ _ => unit end rc0 f0 f2 rc2 hrc2
              with
              | ElRu => fun _ _ => id
              | _ => fun _ _ _ => tt
              end).
    - hnf; move=> A0 Ac0 hA0 A2 Ac2 hA2 f0 rc0 hrc0.
      refine (match hrc0
                    in @elrel A0 Ac0 hA0 A1 Ac1 hA1 f0 rc0
                    return
                    forall (f2 : A1 →mon A2) (rc2 :rcode Ac1 Ac2)
                      (hrc2 :elrel hA1 hA2 f2 rc2),
                    match hA1 as hA1 in el A1 Ac1
                          return
                          rcode Ac0 Ac1 ->
                          forall (f0 : A0 →mon A1) (f2 : A1 →mon A2) (rc2 :rcode Ac1 Ac2),
                          elrel hA1 hA2 f2 rc2 -> Type
                    with
                    | EluOp => fun rc0 f0 f2 rc2 hrc2 =>
                      @elrel A0 Ac0 hA0 A2 Ac2 hA2 (f2 ∘ f0) (transPuOp Ac0 Ac2 rc0 rc2)
                    | _ => fun _ _ _ _ _ => unit end rc0 f0 f2 rc2 hrc2
              with
              | ElRuOp => fun _ _ => id
              | _ => fun _ _ _ => tt
              end).
  Defined.

End Transitivity.

Definition rcode_trans [c0 c1 c2] : rcode c0 c1 -> rcode c1 c2 -> rcode c0 c2.
Proof. refine (ecode rcode_trans_aux _ _ _). Defined.



Definition Elrel_rcode_trans [c0 c1 c2 : code]
           (rc01 : rcode c0 c1) (rc12 : rcode c1 c2):
  Elrel (rcode_trans rc01 rc12) ≈ Elrel rc12 ∘ Elrel rc01.
Proof.
  pose (h := eel rcode_trans_aux (El_valid c1) _ _ (El_valid c0) _ _ (El_valid c2) _ _ (Elrel_valid rc01) _ _ (Elrel_valid rc12)).
  apply map_from_el_elrel in h.
  rhs apply: h.
  unfold map_from_el.
  apply: comp_monotone_right_id_ext.
  apply: comp_monotone_left_id_ext.
  PE.refl.
  apply: Elrel_equiv_valid_bwd.
  apply: Elrel_equiv_valid.
Qed.



Definition variance_eq_dec : forall v0 v1 : variance, {v0 = v1} + {v0 <> v1}.
Proof. decide equality. Defined.


Section SquashRecover.

  Let recovPbin (c0 c1 : code) :=
    { rec : Squash (rcode c0 c1) -> rcode c0 c1
    ≫ forall (rc : rcode c0 c1), Elrel (rec (squash rc)) ≈ Elrel rc }.

  Definition recovP c0 : Type :=
      (forall c1, recovPbin c0 c1) * (forall c1, recovPbin c1 c0).

  Definition recovQ c0 (ihc0 : recovP c0) c1 (ihc1 : recovP c1)
             (rc : rcode c0 c1) :=
    Box ((forall c0' c1' (rc' : rcode c0' c1')
           (sq0 : Squash (rcode c0 c0'))
           (sq1 : Squash (rcode c1 c1')),
            Elrel rc' ∘ Elrel ((ihc0.1 c0')∙1 sq0) ≈
                  Elrel ((ihc1.1 c1')∙1 sq1) ∘ Elrel rc) s/\
          (forall c0' c1' (rc' : rcode c0' c1')
           (sq0 : Squash (rcode c0' c0))
           (sq1 : Squash (rcode c1' c1)),
              Elrel rc ∘ Elrel ((ihc0.2 c0')∙1 sq0) ≈
                    Elrel ((ihc1.2 c1')∙1 sq1) ∘ Elrel rc')).
  Definition recovR A0 Ac0 (ihAc0 : recovP Ac0) (hA0 : el A0 Ac0) : Type :=
    (forall A1 Ac1 (hA1 : el A1 Ac1) (sq : Squash (rcode Ac0 Ac1))
        (rec := (ihAc0.1 Ac1)∙1 sq),
        elrel hA0 hA1 (map_from_el hA0 hA1 (Elrel rec)) rec) *
    (forall A1 Ac1 (hA1 : el A1 Ac1) (sq : Squash (rcode Ac1 Ac0))
       (rec := (ihAc0.2 Ac1)∙1 sq),
        elrel hA1 hA0 (map_from_el hA1 hA0 (Elrel rec)) rec).

  Ltac discriminate_recovPbin :=
    abstract (
        ueconstr;
        [ intros sq ; assert sEmpty as [] by (destruct sq as [h]; inversion h)
        | move=> h; inversion h]).

  Notation ec := (@PiData.ec _ _ _ _ recovP recovQ recovR triv_pred).

  Definition recovPiDataRc (v : variance) (pc0 pc1: PiData.c)
             (ec : ec pc0)
             (sq : Squash (rcode (Cpi v pc0) (Cpi v pc1))) :
    PiData.rc pc0 pc1.
  Proof.
    have @Ac01 : rcode (PiData.Ac pc1) (PiData.Ac pc0).
    1: uref (Spr1 ((PiData.ihAc ec).2 (PiData.Ac pc1)));
      sabstract (
        destruct sq as [h]; inversion h;
        constructor; exact (PiData.Ac01 rc)).
    pose A01 := map_from_el (PiData.hA pc1) (PiData.hA pc0) (Elrel Ac01).
    have @Bc01 : forall a, rcode (PiData.Bc pc0 (A01 a)) (PiData.Bc pc1 a).
    1: {
      move=> a; refine (Spr1 ((PiData.ihBc ec _).1 _) _).
      sabstract (
        destruct sq as [h]; inversion h; constructor;
        apply: rcode_trans; last apply: (PiData.Bc01 rc);
        apply: (PiData.Bcmon pc0);
        apply: from_pteqvl;
        rhs apply: (map_from_el_elrel (PiData.hA01 rc));
        apply: map_from_el_subst;
        apply: (((PiData.ihAc ec).2 _) ∙2 (PiData.Ac01 rc))).
    }
    ueconstr.
    + exact A01.
    + exact Ac01.
    + ueconstr.
      * move=> a.
        refine (map_from_el (PiData.hB pc0 _) (PiData.hB pc1 _) (Elrel (Bc01 _))).
      * sabstract (
          simpl=> a0 a1 a01;
          lhs (apply comp_right; apply imap_map_from_el);
          rhs (apply comp_left; PE.sym ; apply: imap_map_from_el);
          lhs apply: comp_map_from_el;
          rhsrev apply: comp_map_from_el;
          apply: map_from_el_subst;

          PE.sym ; set rc' := (rc' in Elrel rc' ∘ _);
          refine (sprl (unbox (PiData.ihBcmon ec _ _ _)) _ _ rc' _ _)
       ).
    + exact Bc01.
    + refine ((PiData.ihA ec).2 _ _ (PiData.hA pc1) _).
    + move=> a; refine ((PiData.ihB ec _).1 _ _ _ _).
  Defined.

  Definition recovPiDataRcElrel (v : variance) (pc0 pc1: PiData.c)
             (ec : ec pc0) (rc : rcode (Cpi v pc0) (Cpi v pc1)) :
    Elrel (RCpi v (recovPiDataRc v pc0 pc1 ec (squash rc))) ≈ Elrel rc.
  Proof.
    apply: unbox; dependent destruction rc; constructor ; uref Πvar_map_ext.
    - sabstract (
        lhs (apply: map_from_el_subst;
            set z := (z in z ∙1);
            refine (z ∙2 (PiData.Ac01 rc)));
      apply: map_from_el_elrel;
      exact (PiData.hA01 rc)).
    - move=> a /=.
      set z := (z in map_from_el _ _ (Elrel (z ∙1 _))).
      match goal with
      | [|- context [sprl ?t]] => pose (ae := sprl t) end.
      pose (rae := PiData.Bcmon pc0 _ _ ae).
      pose (h' := rcode_trans rae (PiData.Bc01 rc a)).
      lhs (apply: map_from_el_subst ; refine(z ∙2 h'));
      apply: map_from_el_elrel; unfold h';
      apply: (eel rcode_trans_aux);
      [apply: (PiData.hBmon pc0) | exact (PiData.hB01 rc a)].
  Qed.

  (* Symmetric copy of the previous two lemmas :'( *)
  Definition recovPiDataRcSym (v : variance) (pc0 pc1: PiData.c)
             (ec : ec pc0)
             (sq : Squash (rcode (Cpi v pc1) (Cpi v pc0))) :
    PiData.rc pc1 pc0.
  Proof.
    have @Ac01 : rcode (PiData.Ac pc0) (PiData.Ac pc1).
    1: uref (Spr1 ((PiData.ihAc ec).1 (PiData.Ac pc1)));
      sabstract (
        destruct sq as [h]; inversion h;
        constructor; exact (PiData.Ac01 rc)).
    pose A01 := map_from_el (PiData.hA pc0) (PiData.hA pc1) (Elrel Ac01).
    have @Bc01 : forall a, rcode (PiData.Bc pc1 (A01 a)) (PiData.Bc pc0 a).
    1: {
      move=> a; refine (Spr1 ((PiData.ihBc ec _).2 _) _).
      sabstract (
        destruct sq as [h]; inversion h; constructor;
        apply: rcode_trans; last apply: (PiData.Bc01 rc);
        apply: (PiData.Bcmon pc1);
        apply: from_pteqvl;
        rhs apply: (map_from_el_elrel (PiData.hA01 rc));
        apply: map_from_el_subst;
        apply: (((PiData.ihAc ec).1 _) ∙2 (PiData.Ac01 rc))).
    }
    ueconstr.
    + exact A01.
    + exact Ac01.
    + ueconstr.
      * move=> a.
        refine (map_from_el (PiData.hB pc1 _) (PiData.hB pc0 _) (Elrel (Bc01 _))).
      *  sabstract (
          simpl=> a0 a1 a01;
          lhs (apply comp_right; apply imap_map_from_el);
          rhs (apply comp_left; PE.sym ; apply: imap_map_from_el);
          lhs apply: comp_map_from_el;
          rhsrev apply: comp_map_from_el;
          apply: map_from_el_subst;

          PE.sym ;
          apply: (sprr (unbox (PiData.ihBcmon ec _ _ _)))
       ).
    + exact Bc01.
    + refine ((PiData.ihA ec).1 _ _ (PiData.hA pc1) _).
    + move=> a; refine ((PiData.ihB ec _).2 _ _ _ _).
  Defined.


  Definition recovPiDataRcElrelSym (v : variance) (pc0 pc1: PiData.c)
             (ec : ec pc0) (rc : rcode (Cpi v pc1) (Cpi v pc0)) :
    Elrel (RCpi v (recovPiDataRcSym v pc0 pc1 ec (squash rc))) ≈ Elrel rc.
  Proof.
    apply: unbox; dependent destruction rc; constructor ; uref Πvar_map_ext.
    - sabstract (
      lhs (apply: map_from_el_subst;
            set z := (z in z ∙1);
            refine (z ∙2 (PiData.Ac01 rc)));
      apply: map_from_el_elrel;
      exact (PiData.hA01 rc)).
    - move=> a /=.
      set z := (z in map_from_el _ _ (Elrel (z ∙1 _))).
      match goal with
      | [|- context [sprl ?t]] => pose (ae := sprl t) end.
      pose (rae := PiData.Bcmon pc1 _ _ ae).
      pose (h' := rcode_trans rae (PiData.Bc01 rc a)).
      lhs (apply: map_from_el_subst ; refine(z ∙2 h'));
      apply: map_from_el_elrel; unfold h';
      apply: (eel rcode_trans_aux);
      [apply: (PiData.hBmon pc1) | exact (PiData.hB01 rc a)].
  Qed.

  Lemma discard_neqv (v0 v1 : variance) (pc0 pc1 : PiData.c) :
    v0 <> v1 -> recovPbin (Cpi v0 pc0) (Cpi v1 pc1).
  Proof.
    move=> neq; ueconstr;
          [move=> sq; assert sEmpty as []; destruct sq as [h] |move=> h];
          inversion h; exfalso; auto.
  Qed.

  Lemma recovPiDataRcEq
    (v : variance)
    (c0 : PiData.c)
    (ec0 : PiData.ec c0)
    (c1 : PiData.c)
    (ec1 : PiData.ec c1)
    (rc : PiData.rc c0 c1)
    (erc : PiData.erc ec0 ec1 rc)
    (c2 c3 : PiData.c)
    (rc0 : PiData.rc c2 c3)
    (sq0 : Squash (rcode (Cpi v c0) (Cpi v c2)))
    (sq1 : Squash (rcode (Cpi v c1) (Cpi v c3))) :
      Elrel (RCpi v rc0) ∘ Elrel (RCpi v (recovPiDataRc v c0 c2 ec0 sq0))
      ≈ Elrel (RCpi v (recovPiDataRc v c1 c3 ec1 sq1)) ∘ Elrel (RCpi v rc).
  Proof.
    lhs apply: Πvar_map_comp; rhsrev apply: Πvar_map_comp; uref Πvar_map_ext.
    + sabstract (
        lhs (apply: comp_right; PE.sym; apply: (map_from_el_elrel (PiData.hA01 rc0)));
        lhs apply: comp_map_from_el;
        rhs (apply: comp_left; apply: (map_from_el_elrel (PiData.hA01 rc)));
        rhsrev apply: comp_map_from_el;
        apply: map_from_el_subst;
        destruct (PiData.ihAc01 erc) as [[_ h]]; PE.sym ; apply h
      ).
    + move=> a.
      destruct sq0 as [rc']; inversion rc'; subst;
      destruct sq1 as [rc'']; inversion rc''; subst.
      simpl.

      (* cancel out all the map_from_el *)
      lhs (apply: comp_left ; PE.sym; apply: (map_from_el_elrel (PiData.hB01 rc0 a)));
      lhs apply: comp_map_from_el;
      rhs (apply: comp_pointwise_subst;
          first (PE.sym; apply: imap_map_from_el) ;
          apply: comp_pointwise_subst; last PE.refl;
          apply: (map_from_el_elrel (PiData.hB01 rc _)));
      rhs (apply: comp_left; PE.sym ; apply: comp_map_from_el);
      rhsrev apply: comp_map_from_el;
      apply: map_from_el_subst.

      set a' := (a in PiData.ihBc ec1 a).
      destruct (PiData.ihBc01 erc a') as [[h _]].
      specialize (h _ _ (PiData.Bc01 rc0 a)).


      apply: pointwise_equiv_trans.
      2:{ apply: comp_left; apply: h; constructor.
          apply: rcode_trans; last apply: (PiData.Bc01 rc1 _).
          uref (PiData.Bcmon c0).
          unfold a'; move: {a'}a.
          set ihrc1 := (rc in Elrel rc).
          change _ with
              (forall a, (PiData.A01 rc ∘ map_from_el (PiData.hA c3) (PiData.hA c1) (Elrel ihrc1))  a ≤ (PiData.A01 rc1 ∘ PiData.A01 rc0) a).
          apply: from_pteqvl.


          destruct (PiData.ihAc01 erc) as [[_ h]];
          specialize (h _ _ (PiData.Ac01 rc0));
          specialize (h (squash (PiData.Ac01 rc2)) (squash (PiData.Ac01 rc1))).
          rhs (lhs exact (map_from_el_subst (PiData.hA c3) (PiData.hA c0) h)).
          1:{
            rhs apply: comp_map_from_el.
            apply: comp_left ;
            PE.sym; apply: map_from_el_elrel; exact (PiData.hA01 rc).
          }

          lhsrev apply: comp_map_from_el.
          2: {
            apply: comp_pointwise_subst.
            apply: map_from_el_elrel; exact (PiData.hA01 rc0).
            lhs apply: map_from_el_subst.
            set z := (z in z ∙1 _).
            refine (z ∙2 _).
            apply: map_from_el_elrel.
            exact (PiData.hA01 rc1).
          }
      }

      rhs apply: comp_assoc.
      apply: comp_right.
      match goal with
      | [|- _ ≈ Elrel ?rc ∘ Elrel ?rc'] =>
        set rcX := rc ; set rcY := rc'
      end.
      set z := (z in Elrel (z ∙1 _) ≈ _).
      lhs refine (z ∙2 (rcode_trans rcY rcX)).
      apply: Elrel_rcode_trans.
  Qed.


  Lemma recovPiDataRcSymEq
    (v : variance)
    (c0 : PiData.c)
    (ec0 : PiData.ec c0)
    (c1 : PiData.c)
    (ec1 : PiData.ec c1)
    (rc : PiData.rc c0 c1)
    (erc : PiData.erc ec0 ec1 rc)
    (c2 c3 : PiData.c)
    (rc0 : PiData.rc c2 c3)
    (sq0 : Squash (rcode (Cpi v c2) (Cpi v c0)))
    (sq1 : Squash (rcode (Cpi v c3) (Cpi v c1))) :
    Elrel (RCpi v rc) ∘ Elrel (RCpi v (recovPiDataRcSym v c0 c2 ec0 sq0))
    ≈ Elrel (RCpi v (recovPiDataRcSym v c1 c3 ec1 sq1)) ∘ Elrel (RCpi v rc0).
  Proof.
    apply: pointwise_equiv_trans; first apply: Πvar_map_comp.
    apply: pointwise_equiv_trans; last (apply: pointwise_equiv_sym ; apply: Πvar_map_comp).
    uref Πvar_map_ext.
    sabstract (
    destruct (PiData.ihAc01 erc) as [[h _]];
    destruct sq0 as [rc']; inversion rc'; subst;
    destruct sq1 as [rc'']; inversion rc''; subst;
    specialize (h _ _ (PiData.Ac01 rc0));
    specialize (h (squash (PiData.Ac01 rc2)) (squash (PiData.Ac01 rc1)));
    (* ok, we almost have what we need up to some map_from_el tweaks *)
    apply: pointwise_equiv_sym; apply: pointwise_equiv_trans;
      last (apply:pointwise_equiv_trans;
        first exact (map_from_el_subst (PiData.hA c1) (PiData.hA c2) h));
    [
    apply: pointwise_equiv_trans; last apply: comp_map_from_el;
    first (apply: comp_pointwise_subst; first apply: pointwise_equiv_refl;
            apply: pointwise_equiv_sym; apply: map_from_el_elrel; exact (PiData.hA01 rc0))
    |
    apply: pointwise_equiv_trans;
    first (apply: pointwise_equiv_sym; apply: comp_map_from_el);
    last (apply: comp_pointwise_subst; last apply: pointwise_equiv_refl;
          apply: map_from_el_elrel; exact (PiData.hA01 rc))
    ]
    ).

    move=> a.
    destruct sq0 as [rc']; inversion rc'; subst;
    destruct sq1 as [rc'']; inversion rc''; subst.
    simpl.

    apply: pointwise_equiv_trans.
    apply: comp_pointwise_subst; first apply: pointwise_equiv_refl.
    apply: pointwise_equiv_sym.
    apply: (map_from_el_elrel (PiData.hB01 rc a)).

    apply: pointwise_equiv_trans.
    apply: comp_map_from_el.

    apply: pointwise_equiv_trans.
    2:{
      apply: comp_pointwise_subst;
      last (apply: comp_pointwise_subst; last apply: pointwise_equiv_refl).
      apply: pointwise_equiv_sym. apply: imap_map_from_el.
      apply: (map_from_el_elrel (PiData.hB01 rc0 _)).
    }

    apply: pointwise_equiv_trans.
    2:{ apply comp_pointwise_subst.
        apply: pointwise_equiv_refl.
        apply: pointwise_equiv_sym.
        apply: comp_map_from_el.
    }

    apply: pointwise_equiv_trans.
    2:  apply: pointwise_equiv_sym; apply: comp_map_from_el.

    apply: map_from_el_subst.

    set a' := (a in PiData.ihBc ec1 a).
    destruct (PiData.ihBc01 erc a') as [[_ h]].

    apply: pointwise_equiv_trans.
    2:{ apply: comp_pointwise_subst.
        apply: pointwise_equiv_refl.
        apply: h; constructor.
        apply: rcode_trans; last apply: (PiData.Bc01 rc1 _).
        refine (PiData.Bcmon _ _ _ _).
        unfold a'.
        move: {a'}a.
        set ihrc1 := (rc in Elrel rc).
        change _ with
            (forall a, (PiData.A01 rc0 ∘ map_from_el (PiData.hA c1) (PiData.hA c3) (Elrel ihrc1))  a ≤ (PiData.A01 rc1 ∘ PiData.A01 rc) a).
        apply: from_pteqvl.


      destruct (PiData.ihAc01 erc) as [[h _]];
      specialize (h _ _ (PiData.Ac01 rc0));
      specialize (h (squash (PiData.Ac01 rc2)) (squash (PiData.Ac01 rc1))).
    (* apply: pointwise_equiv_sym; *) apply: pointwise_equiv_trans;
      last (apply:pointwise_equiv_trans;
        first exact (map_from_el_subst (PiData.hA c1) (PiData.hA c2) h)).

    apply: pointwise_equiv_trans; last apply: comp_map_from_el.
    apply: comp_pointwise_subst; first apply: pointwise_equiv_refl.
    apply: pointwise_equiv_sym; apply: map_from_el_elrel.
    exact (PiData.hA01 rc0).

    apply: pointwise_equiv_trans;
    first (apply: pointwise_equiv_sym; apply: comp_map_from_el).
    2: { apply: comp_pointwise_subst.
          apply: map_from_el_elrel; exact (PiData.hA01 rc).
          apply: pointwise_equiv_trans; first apply: map_from_el_subst.
          set z := (z in z ∙1 _).
          refine (z ∙2 _).
          apply: map_from_el_elrel.
          exact (PiData.hA01 rc1).
    }
    }
    apply: pointwise_equiv_trans; last apply: comp_assoc.
    apply: comp_pointwise_subst; last apply: pointwise_equiv_refl.
    match goal with
    | [|- _ ≈ Elrel ?rc ∘ Elrel ?rc'] =>
      set rcX := rc ; set rcY := rc'
    end.
    set z := (z in Elrel (z ∙1 _) ≈ _).
    apply: pointwise_equiv_trans.
    refine (z ∙2 (rcode_trans rcY rcX)).

    apply: Elrel_rcode_trans.
  Qed.


  Ltac solve_recovP :=
    split=> -[]; try (intros; discriminate_recovPbin);
           (ueconstr; [move=> ?; constructor|
                      sabstract (
                          move=> rc; apply unbox; dependent destruction rc;
                                constructor ; apply: pointwise_equiv_refl)]).

  Ltac solve_recovQ :=
    constructor; split;
       (move=> ? ? rc'; apply: unbox ; dependent destruction rc' ; constructor; try (move=> ? ?; PE.refl) ;
        intros sq ?; assert sEmpty as [] by (destruct sq as [h]; inversion h)).

  Ltac solve_recovR :=
    split; simpl;
    move=> A Ac hA; dependent destruction hA;
          try (intros sq; assert sEmpty as [] by (destruct sq as [h]; inversion h));
          move=> ? /=; constructor.


  Definition recov_aux :
    @elim_all_code_res recovP recovQ recovR triv_pred.
  Proof.
    uref @elim_all_code. all: try discharge_triv_pred.

    1,3,4: solve_recovP.
    2,4,5: solve_recovQ.
    3,5,6: solve_recovR.

    (* recovP *)
    - move=> v0 pc0 ec; split=> -[|v1 pc1| |]; try discriminate_recovPbin.
      * case: (variance_eq_dec v0 v1)=> [{v1}<-|]; last apply: discard_neqv.
        ueconstr; first (move=> sq; ueconstr; apply: recovPiDataRc; eassumption).
        apply: recovPiDataRcElrel.
      * case: (variance_eq_dec v1 v0)=> [{v0}<-|]; last apply: discard_neqv.
        ueconstr; first (move=> sq; ueconstr; apply: recovPiDataRcSym; eassumption).
        apply: recovPiDataRcElrelSym.


    (* recovQ *)
    - move=> v c0 ec0 c1 ec1 rc erc.
      constructor; split.
      * move=> c0' c1' rc' ; apply: unbox ; dependent destruction rc'; constructor.
        1,3,4: move=> sq ?; assert sEmpty as [] by (destruct sq as [h]; inversion h).
        move=> sq0 sq1 /=; set vdec := variance_eq_dec _ _; dependent destruction vdec.
        2: assert sEmpty as [] by (destruct sq0 as [h]; exfalso; apply n; by inversion h).
        dependent destruction e; simpl.
        apply: recovPiDataRcEq; eassumption.
      * (* Symmetric of the previous case, to factor out *)
        move=> c0' c1' rc' ; apply: unbox ; dependent destruction rc'; constructor.
        1,3,4: move=> sq ?; assert sEmpty as [] by (destruct sq as [h]; inversion h).
        move=> sq0 sq1 /=; set vdec := variance_eq_dec _ _; dependent destruction vdec.
        2: assert sEmpty as [] by (destruct sq0 as [h]; exfalso; apply n; by inversion h).
        dependent destruction e; simpl.
        apply: recovPiDataRcSymEq; eassumption.


   (* recovR *)
   - move=> v pc0 ec; split.
     * move=> A Ac hA; dependent destruction hA;
            try (move=> sq; assert sEmpty as [] by (destruct sq as [h]; inversion h)).
       simpl.
       set vdec := variance_eq_dec _ _.
       dependent destruction vdec.
       2: move=> sq; assert sEmpty as [] by (destruct sq as [h]; exfalso; apply n; by inversion h).
       dependent destruction e.
       move=> ?; constructor.
     * move=> A Ac hA; dependent destruction hA;
            try (move=> sq; assert sEmpty as [] by (destruct sq as [h]; inversion h)).
       simpl.
       set vdec := variance_eq_dec _ _.
       dependent destruction vdec.
       2: move=> sq; assert sEmpty as [] by (destruct sq as [h]; exfalso; apply n; by inversion h).
       dependent destruction e.
       move=> ?; constructor.

  Defined.

End SquashRecover.


Definition recov_rcode [c0 c1] (sq : Squash (rcode c0 c1)) : rcode c0 c1 :=
  ((ecode recov_aux c0).1 c1) ∙1 sq.

Lemma Elrel_recov_rcode [c0 c1] (rc : rcode c0 c1) :
  Elrel (recov_rcode (squash rc)) ≈ Elrel rc.
Proof.
  unfold recov_rcode.
  set z := (z in z ∙1 _).
  refine (z ∙2 _).
Qed.

(* Thanks to the squash, we can now prove that there is
   extensionally only one function betwen two codes *)
Definition Elrel_irr [c0 c1] (rc rc' : rcode c0 c1) :
  Elrel rc ≈ Elrel rc'.
Proof.
  lhsrev (apply: Elrel_recov_rcode).
  rhs (apply: Elrel_recov_rcode).
  PE.refl.
Qed.


Lemma elrel_El_valid [c0 c1] [f : El c0 →mon El c1] [rc : rcode c0 c1] :
  elrel (El_valid c0) (El_valid c1) f rc -> Elrel rc ≈ f.
Proof.
  move=> /map_from_el_elrel h.
  rhs (apply: h).
  apply: comp_monotone_right_id_ext; last apply: Elrel_equiv_valid.
  apply: comp_monotone_left_id_ext; last apply: Elrel_equiv_valid_bwd.
  PE.refl.
Qed.


Definition rcode_refl c : rcode c c := ecode rcode_refl_aux c.

Lemma Elrel_rcode_refl c : Elrel (rcode_refl c) ≈ idmon (El c).
Proof.
  destruct (eel rcode_refl_aux (El_valid c)) as [f [hf eq]].
  rhs (apply: eq).
  apply: (elrel_El_valid hf).
Qed.




(** ** Main goal of this file *)

Program Definition Univ := mkPreorder code (fun c0 c1 => Squash (rcode c0 c1)) _ _.
Next Obligation.
  move=> c0 c1 c2 /= [rc01] [rc12]; exact (squash (rcode_trans rc01 rc12)).
Qed.
Next Obligation.
  move=> c /=; exact (squash (ecode rcode_refl_aux c)).
Qed.


Program Definition Elmon : IPreorder Univ :=
  mkIPreorder Univ El (fun _ _ sq => Elrel (recov_rcode sq)) _ _.
Next Obligation.
  move=> c0 c1 c2 [rc01] [rc12] /=.
  change (Univ_obligation_1 _ _ _ _ _) with (squash (rcode_trans rc01 rc12)).
  lhs (apply: Elrel_recov_rcode).
  lhs (apply: Elrel_rcode_trans).
  apply: comp_monotone_split; PE.sym; apply: Elrel_recov_rcode.
Qed.
Next Obligation.
  move=> c /=.
  change (Univ_obligation_2 _) with (squash (ecode rcode_refl_aux c)).
  lhs (apply: Elrel_recov_rcode).
  destruct (eel rcode_refl_aux (El_valid c)) as [f [hf eq]].
  rhs (apply: eq).
  apply: elrel_El_valid hf.
Qed.


End UnivParam.
