From SubCIC Require Import Preamble.
From SubCIC.Preorder Require Import PreorderSProp.

Set Universe Polymorphism.
Set Primitive Projections.


Record code_pack@{uc} :=
  mkCodePack
    { cp_code : Type@{uc}
    ; cp_rcode : cp_code -> cp_code -> Type@{uc}
    ; cp_el : Preorder@{uc} -> cp_code -> Type@{uc}
    ; cp_elrel : forall [A0 : Preorder] [Ac0 : cp_code] (hA0 : cp_el A0 Ac0)
                   [A1 : Preorder] [Ac1 : cp_code] (hA1 : cp_el A1 Ac1)
                   (f : A0 →mon A1) (rc : cp_rcode Ac0 Ac1), Type@{uc}
    }.

Section types.
  Universe uc.
  Context {cp : code_pack@{uc}}.

  Notation code := (cp_code cp).
  Notation rcode := (cp_rcode cp).
  Notation el := (cp_el cp).
  Notation elrel := (cp_elrel cp).

  Record pretype :=
    mkPretype
      { A :> Preorder@{uc}
      ; Ac : code
      ; hA : el A Ac }.

  Record prefam {X : pretype} :=
    mkPrefam
      { B :> IPreorder X
      ; Bc : A X -> code
      ; hB : forall (a : A X), el (B a) (Bc a)
      ; Bcmon : forall [a0 a1 : A X] (a01 : a0 ≤ a1), rcode (Bc a0) (Bc a1)
      ; hBmon : forall [a0 a1 : A X] (a01 : a0 ≤ a1), elrel (hB a0) (hB a1) (imap B a01) (Bcmon  a01)
      }.

  Arguments prefam : clear implicits.

  Record premorph {X0 X1 : pretype} :=
    mkPremorph
      { A01 :> X0 →mon X1
      ; Ac01 : rcode (Ac X0) (Ac X1)
      ; hA01 : elrel (hA X0) (hA X1) A01 Ac01
      }.

  Arguments premorph : clear implicits.

  Record preimorph {X0} {Y0 : prefam X0} {X1} {Y1 : prefam X1} {X01 : premorph X0 X1} :=
    mkPreImorph
      { B01 :> forall (a0 : A X0), Y0 a0 →mon Y1 (A01 X01 a0)
      ; Bc01 : forall (a0 : A X0), rcode (Bc Y0 a0) (Bc Y1 (A01 X01 a0))
      ; hB01 : forall (a0 : A X0),
          elrel (hB Y0 a0) (hB Y1 (A01 X01 a0)) (B01 a0) (Bc01 a0)
      }.
End types.

Arguments prefam : clear implicits.
Arguments prefam [_] _.
Arguments premorph : clear implicits.
Arguments premorph [_] _ _.
Arguments preimorph : clear implicits.
Arguments preimorph [_ _] _ [_] _ _.


Arguments mkPrefam [_ _] _ _ _ _ _.
Arguments mkPremorph [_ _ _] _ _ _.
Arguments mkPreImorph [_ _ _ _ _ _] _ _ _.


Section EliminatorPredTypes.
  Universe uc.
  Context {cp : code_pack@{uc}}.

  Notation code := (cp_code cp).
  Notation rcode := (cp_rcode cp).
  Notation el := (cp_el cp).
  Notation elrel := (cp_elrel cp).

  Definition elimPTy := code -> Type.
  Definition elimQTy (P : elimPTy) :=
    forall c0 (ihc0 : P c0) c1 (ihc1 : P c1), rcode c0 c1 -> Type.
  Definition elimRTy (P : elimPTy) := forall A Ac (ihAc : P Ac), el A Ac -> Type.
  Definition elimSTy (P : elimPTy) (Q : elimQTy P) (R : elimRTy P) :=
    forall A0 Ac0 (ihAc0 : P Ac0)
      (hA0 : el A0 Ac0) (ihA0 : R _ _ ihAc0 hA0)
      A1 Ac1 (ihAc1 : P Ac1)
      (hA1 : el A1 Ac1) (ihA1 : R _ _ ihAc1 hA1)
      (A01 : A0 →mon A1) (Ac01 : rcode Ac0 Ac1)
      (ihAc01 : Q _ ihAc0 _ ihAc1 Ac01),
      elrel hA0 hA1 A01 Ac01 -> Type.

  (* To be used as *)
  Context {P : elimPTy} {Q : elimQTy P} {R : elimRTy P} {S : elimSTy P Q R}.
  Arguments Q [_] _ [_] _ _.
  Arguments R [_ _] _ _.
  Arguments S [_ _ _ _] _ [_ _ _ _] _ [_ _] _ _.

  Notation pretype := (pretype (cp:=cp)).

  Record epretype {X : pretype} :=
    mkEpretype
      { ihAc : P (Ac X)
      ; ihA : R ihAc (hA X)
      }.

  Arguments epretype : clear implicits.

  Record eprefam {X : pretype} {Y : prefam X} :=
    mkEprefam
      { ihBc : forall a, P (Bc Y a)
      ; ihB : forall a, R (ihBc a) (hB Y a)
      ; ihBcmon : forall [a0 a1] (a01 : a0 ≤ a1),
          Q (ihBc a0) (ihBc a1) (Bcmon Y a01)
      ; ihBmon : forall [a0 a1] (a01 : a0 ≤ a1),
          S (ihB a0) (ihB a1) (ihBcmon a01) (hBmon Y a01)
      }.

  Arguments eprefam : clear implicits.

  Record epremorph {X0} {eX0 : epretype X0} {X1} {eX1 : epretype X1} {X01 : premorph X0 X1} :=
    mkEpremorph
      { ihAc01 : Q (ihAc eX0) (ihAc eX1)  (Ac01 X01)
      ; ihA01 : S (ihA eX0) (ihA eX1) ihAc01 (hA01 X01)
      }.

  Arguments epremorph : clear implicits.

  Record epreimorph
         {X0} {eX0 : epretype X0} {Y0} {eY0 : eprefam X0 Y0}
         {X1} {eX1 : epretype X1} {Y1} {eY1 : eprefam X1 Y1}
         {X01 : premorph X0 X1} {Y01 : preimorph Y0 Y1 X01} :=
    mkEpreImorph
      { ihBc01 : forall a0, Q (ihBc eY0 a0) (ihBc eY1 (A01 X01 a0))  (Bc01 Y01 a0)
      ; ihB01 : forall a0,
          S (ihB eY0 a0) (ihB eY1 (A01 X01 a0)) (ihBc01 a0) (hB01 Y01 a0)
      }.

End EliminatorPredTypes.

