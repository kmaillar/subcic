From SubCIC Require Import Preamble.
From SubCIC.Preorder Require Import PreorderSProp.

Notation Π A B := (forall (a : A), B a).

Inductive variance := Cov | Contra | Biv.

Definition vrel {A : Preorder} (v:variance) (x y : A) :=
  match v with
  | Cov => x ≤ y
  | Contra => y ≤ x
  | Biv => equiv x y
  end.

Lemma vrefl {A : Preorder}  (v : variance) (x : A) : vrel v x x.
Proof. case: v; last split; apply: reflexive. Defined.

Lemma vtrans {A : Preorder}  (v : variance) (x y z : A) :
  vrel v x y -> vrel v y z -> vrel v x z.
Proof.
  case: v; last (move=> [? ?] [? ?] ; split);
  move=> * ; apply: transitive; eassumption.
Defined.

Record var_Pi (v:variance) A (B : IPreorder A) : Type :=
  mkVarPi
    { vp_fun :> Π A B
    ; vp_mon : forall x y (xy : x ≤[A] y), vrel v (imap B xy (vp_fun x)) (vp_fun y) }.

Arguments vp_fun [_ _ _] _.
Arguments vp_mon [_ _ _] _ [_ _] _.

Obligation Tactic := idtac.

Program Definition Πvar (v:variance) (A : Preorder) (B : IPreorder A)
  : Preorder :=
  mkPreorder
    (var_Pi v A B)
    (fun f g => forall x, f x ≤ g x)
    _ _.
Next Obligation.
  move=> v A B f g h /= fg gh x; apply: transitive; eauto.
Qed.
Next Obligation.
  move=> [] ? ? f a; apply: transitive; try split ; try exact (vp_mon f (reflexive a)).
  - (* Cov *) move: (iid _ (f a)) => /sprr //=.
  - (* Contra *) move: (iid _ (f a)) => /sprl //=.
  - (* Biv 1 *) move: (iid _ (f a)) => [_ h] /=; exact h.
  - (* Biv 2 *) move: (iid _ (f a)) => [h _] /=; exact h.
Qed.

(* op (Πvar Cov A B) ~ Πvar Cov (op A) (op_contra B) ~ Πvar Contra A (iop B) *)

Section Πmap.
  Context (v : variance)
          {A0 A1 : Preorder} {B0 : IPreorder A0} {B1 : IPreorder A1}
          (A01 : A1 →mon A0) (B01 : natTrans (pb A01 B0) B1).

  Definition Πvar_map : Πvar v A0 B0 →mon Πvar v A1 B1.
  Proof.
    simple refine (mkMonMap _ (Πvar v _ _) (fun f => mkVarPi v _ _ _ _) _).
    - exact (fun a1 => B01 a1 (f (A01 a1))).
    - move=> x y xy /=.
      set z := v; case: v @z f => z f /=; last split; set x' := f _.
      all: apply: transitive;
      try apply: (sprl (ntnatural B01 _ _));
      try apply: (sprr (ntnatural B01 _ _));
      simpl; apply: monotone; try apply: (vp_mon (v:=z)).
      all: (* 2 cases for Biv *)
        move: (vp_mon (v:=z) f (monotone A01 xy)) => [? ?] //.
    - move=> /= f g fg a1 /=; apply: monotone; apply: fg.
  Defined.

End Πmap.



(** ** TO FILL IN AND CLEAN UP **)


Definition Πvar_map_comp
           [v A0 A1 A2] (A01 : A1 →mon A0) (A12 : A2 →mon A1)
           [B0 B1 B2] (B01 : natTrans (pb A01 B0) B1)
           (B12: natTrans (pb A12 B1) B2):
  Πvar_map v A12 B12 ∘ Πvar_map v A01 B01 ≈
           Πvar_map v (A01 ∘ A12) (comp_natTrans_pb B01 B12).
Proof. PE.refl. Qed.

From Coq.Program Require Import Equality.

Definition Πvar_map_id0 [v A B]:
  Πvar_map v (idmon A) (natTrans_id_skew B) ≈ idmon _.
Proof. move=> f ; split=> a /=; apply: reflexive. Qed.

Definition Πvar_map_ext [v A0 A1] [A01 A01' : A1 →mon A0]
           [B0 B1 B01 B01']
           (Aeq : A01 ≈ A01')
           (Beq : @natTrans_equiv _ (pb A01 B0) B1 B01
                                  (comp_natTrans (pb_eq Aeq B0) B01')) :
  Πvar_map v A01 B01 ≈ Πvar_map v A01' B01'.
Proof.
  move=> f; split=> a /=.
  - apply: transitive; first apply: sprl (Beq a _).
    simpl; apply: monotone.
    case: v f => /= f.
    + apply: (vp_mon f).
    + apply: transitive;
      first (apply: monotone; apply: (vp_mon f); exact (sprr (Aeq a))).
      apply: transitive; first (apply: (sprr (icomp (sprr (Aeq a)) (sprl (Aeq a)) _))).
      apply: (sprl (iid _ _)).
    + apply: (sprl (vp_mon f _)).
  - apply: transitive; last apply: sprr (Beq a _).
    simpl; apply: monotone.
    case: v f => /= f.
    + apply: transitive;
      last (apply: monotone; apply: (vp_mon f); exact (sprr (Aeq a))).
      apply: transitive;
      last apply: (sprl (icomp (sprr (Aeq a)) (sprl (Aeq a)) _)).
      apply: (sprr (iid _ _)).
    + apply: (vp_mon f).
    + apply: (sprr (vp_mon f _)).
Qed.

Import PENotations.

Definition Πvar_map_id
           [v A] (A01 : A →mon A) B (B01 : natTrans (pb A01 B) B)
           (Aeq : A01 ≈ idmon A)
           (Beq :  natTrans_equiv B01 (comp_natTrans (pb_eq Aeq B) (natTrans_id_skew B))) :
  Πvar_map v A01 B01 ≈ idmon _.
Proof.
  rhs (apply: Πvar_map_id0).
  apply: Πvar_map_ext; eassumption.
Qed.

Module PiData.
  (*Necessary to pass strict positivity *)
  Local Set Primitive Projections.
  Section Codes.

    Universe u.
    Context {code : Type@{u}}
            {rcode : code -> code -> Type@{u}}
            {el : Preorder@{u} -> code -> Type@{u}}
            {elrel : forall A0 Ac0 (hA0 : el A0 Ac0)
                       A1 Ac1 (hAc1 : el A1 Ac1),
                       A0 →mon A1 ->
                       rcode Ac0 Ac1 -> Type@{u}}.
    Arguments elrel [_ _] _ [_ _] _ _ _.

    Record c :=
      mkC
        { Ac : code
        ; A : Preorder
        ; hA : el A Ac
        ; B : IPreorder A
        ; Bc : forall a, code
        ; hB : forall a, el (B a) (Bc a)
        ; Bcmon : forall a0 a1 (a01 : a0 ≤[A] a1), rcode (Bc a0) (Bc a1)
        ; hBmon : forall a0 a1 (a01 : a0 ≤[A] a1),
            elrel (hB a0) (hB a1) (imap B a01) (Bcmon a0 a1 a01)}.

    Record rc {c0 c1 : c} :=
      mkRC
        { A01 : (A c1)→mon (A c0)
        ; Ac01 : rcode (Ac c1)(Ac c0)
        ; hA01 : elrel (hA c1) (hA c0) A01 Ac01
        ; B01 : natTrans (pb A01 (B c0)) (B c1)
        ; Bc01 : forall a1, rcode (Bc c0 (A01 a1)) (Bc c1 a1)
        ; hB01 : forall a1, elrel (hB c0 (A01 a1)) (hB c1 a1) (B01 a1) (Bc01 a1)}.


    Section ElimCode.
      Context {P : code -> Type}
              {Q : forall c0 (ihc0 : P c0) c1 (ihc1 : P c1), @rcode c0 c1 -> Type }
              {R : forall A Ac (ihAc : P Ac), el A Ac -> Type}.
      Arguments Q [_] _ [_] _ _.
      Arguments R [_ _] _ _.

      Context {S : forall A0 Ac0 (ihAc0 : P Ac0)
                     (hA0 : el A0 Ac0) (ihA0 : R ihAc0 hA0)
                     A1 Ac1 (ihAc1 : P Ac1)
                     (hA1 : el A1 Ac1) (ihA1 : R ihAc1 hA1)
                     (A01 : A0 →mon A1) (Ac01 : rcode Ac0 Ac1)
                     (ihAc01 : Q ihAc0 ihAc1 Ac01),
                  elrel hA0 hA1 A01 Ac01 -> Type}.
      Arguments S [_ _ _ _] _ [_ _ _ _] _ [_ _] _ _.


      Record ec {c : c} :=
        mkEC
          { ihAc : P (Ac c)
          ; ihA : R ihAc (hA c)
          ; ihBc : forall a, P (Bc c a)
          ; ihB : forall a, R (ihBc a) (hB c a)
          ; ihBcmon : forall a0 a1 a01, Q (ihBc a0) (ihBc a1) (Bcmon c a0 a1 a01)
          ; ihBmon : forall a0 a1 a01, S (ihB a0) (ihB a1) (ihBcmon a0 a1 a01)
                                    (hBmon c a0 a1 a01)
          }.

      Record erc {c0} {ec0 : @ec c0} {c1} {ec1 : @ec c1} {rc : @rc c0 c1} :=
        mkERC
          { ihAc01 : Q (ihAc ec1) (ihAc ec0) (Ac01 rc)
          ; ihA01 : S (ihA ec1) (ihA ec0) ihAc01 (hA01 rc)
          ; ihBc01 : forall a1 (a0 := A01 rc a1), Q (ihBc ec0 a0) (ihBc ec1 a1) (Bc01 rc a1)
          ; ihB01 : forall a1 (a0 := A01 rc a1), S (ihB ec0 a0) (ihB ec1 a1) (ihBc01 a1) (hB01 rc a1)
          }.
    End ElimCode.

  End Codes.

  Arguments rc : clear implicits.
  Arguments rc [_ _ _ _] _ _.
  Arguments ec : clear implicits.
  Arguments ec [_ _ _ _] [_ _ _ _] _.
  Arguments erc : clear implicits.
  Arguments erc [_ _ _ _] [_ _ _ _] [_] _ [_] _ _.

  (* Ltac destr := *)
  (*   repeat lazymatch goal with *)
  (*   | [ H : c _ _ |- _] => destruct H *)
  (*   | [ H : rc _ _ _ _ |- _] => destruct H *)
  (*   | [ H : ec _ _ _ _ _ |- _] => destruct H *)
  (*   | [ H : erc _ _ _ _ _ _ _ _ _  |- _] => destruct H *)
  (*   (* | [ H : gel _ _ _ _ |- _ ] => destruct H *) *)
  (*   | [ H : gelrel _ _ _ _ _ _ _ _ |- _ ] => destruct H *)
  (*   end. *)

End PiData.
