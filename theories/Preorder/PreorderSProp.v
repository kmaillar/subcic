From SubCIC Require Import Preamble.

Export SPropNotations.
Set Primitive Projections.
Set Universe Polymorphism.

(** **  Preorders with relations in SProp *)


(* Not really used... *)
Definition srel A B := A -> B -> SProp.
Definition ext_equiv {A B} (R R' : srel A B) := forall a b, R a b s<-> R' a b.
Definition ext {A B} (R : srel A B) := { R' : srel A B ≫ ext_equiv R R' }.
Definition swap {X Y} (R : srel X Y) : srel Y X := fun y x => R x y.
Definition isrel {A0 A1} (RA : srel A0 A1) B0 B1 :=
  forall {a0 a1} (a01 : RA a0 a1), srel (B0 a0) (B1 a1).
Arguments isrel [_ _] _ _ _.



Record Preorder@{u} :=
  mkPreorder
    { carrier :> Type@{u}
    ; rel : carrier -> carrier -> SProp
    ; transitive : forall a b c, rel a b -> rel b c -> rel a c
    ; reflexive : forall a, rel a a }.

Set Printing Universes.

Notation "a ≤ b" := (@rel _ a b) (at level 70).
Arguments transitive [_ _ _ _] _ _.
Arguments reflexive [_] _.

Notation "xy ∙ yz" := (transitive xy yz) (at level 75).

Ltac trans := refine (transitive _ _).
Ltac refl := apply: reflexive.

Definition discr A : Preorder :=
  mkPreorder A sEq (@sEq_trans _) sEq_refl.

Program Definition op (H : Preorder) : Preorder :=
  mkPreorder H (fun x y => y ≤ x) _ _.
Next Obligation. move=> /= *; trans; eassumption. Qed.
Next Obligation. move=> *; refl. Qed.

Notation "a ≤[ P ] b" := (rel P a b) (at level 70).

Lemma le_op (A : Preorder) (x y : A) : x ≤ y -> y ≤[ op A ] x.
Proof. trivial. Defined.



(** **  Monotone map between preorders *)

Record monotone_map (A B : Preorder) :=
  mkMonMap
    { map :> A -> B
    ; monotone : forall a0 a1, a0 ≤ a1 -> map a0 ≤ map a1
    }.

Arguments monotone [_ _] _ [_ _] _.

Notation "A →mon B" := (monotone_map A B) (at level 70).

Definition op_mon {A B : Preorder} (f : A →mon B) : op A →mon op B :=
  mkMonMap (op A) (op B) f (fun _ _ a01 => monotone f a01).

Definition idmon (A : Preorder) : A →mon A :=
  mkMonMap A A id (fun _ _ => id).

Definition comp_monotone [A B C : Preorder] (g : B →mon C) (f : A →mon B) :
  A →mon C :=
  mkMonMap A C (g \o f) (fun _ _ a01 => monotone g (monotone f a01)).

Notation "g ∘ f" := (comp_monotone g f) (at level 50).

Definition equiv {A : Preorder} (x y : A) := x ≤ y s/\ y ≤ x.

Definition equiv_refl [A : Preorder] (a : A) : equiv a a.
Proof. split; apply: reflexive. Qed.

Definition equiv_sym [A : Preorder] [a b : A] : equiv a b -> equiv b a.
Proof. move=> []; by split. Qed.

Definition equiv_trans [A : Preorder] [a b c : A] : equiv a b -> equiv b c -> equiv a c.
Proof. move=> [? ?] [? ?] ; split; apply: transitive; eassumption. Qed.


Definition pointwise_equiv {A B : Preorder} (f g : A →mon B) :=
  forall a, equiv (f a) (g a).

Notation "f ≈ g" := (pointwise_equiv f g) (at level 55).

Definition pointwise_equiv_refl [A B] (f : A →mon B) : f ≈ f.
Proof. move=> x; apply: equiv_refl. Qed.

Definition pointwise_equiv_sym [A B] [f g : A →mon B] : f ≈ g -> g ≈ f.
Proof. move=> fg a; move: (fg a); apply: equiv_sym. Qed.

Definition pointwise_equiv_trans [A B] [f g h : A →mon B] :
  f ≈ g -> g ≈ h -> f ≈ h.
Proof. move=> fg gh a; move: (fg a) (gh a); apply: equiv_trans. Qed.

Definition from_pteqvl [A0 A1 : Preorder] [f g : A0 →mon A1]:
  f ≈ g -> forall a, f a ≤ g a.
Proof. move=> fg a; move: (fg a)=> []//=. Qed.

Definition from_pteqvr [A0 A1 : Preorder] [f g : A0 →mon A1]:
  f ≈ g -> forall a, g a ≤ f a.
Proof. move=> fg a; move: (fg a)=> []//=. Qed.


Module PE.
  Ltac trans := apply: pointwise_equiv_trans.
  Ltac sym := apply: pointwise_equiv_sym.
  Ltac refl := apply: pointwise_equiv_refl.
End PE.

Module PENotations.
  (* putting tactic argument at level 2 so that
     "lhs apply: foo ; bar" is parsed as "(lhs apply: foo) ; bar" *)
  Tactic Notation "lhs" tactic2(t) := (PE.trans ; first t).
  Tactic Notation "lhsrev" tactic2(t) := (PE.trans ; first (PE.sym ; t)).
  Tactic Notation "rhs" tactic2(t) := (PE.trans ; last t).
  Tactic Notation "rhsrev" tactic2(t) := (PE.trans ; last (PE.sym ; t)).
End PENotations.

Import PENotations.

(* Lemmas on composition *)

Lemma comp_monotone_split [A B C] (f0 g0 : A →mon B) (f1 g1 : B →mon C):
  f0 ≈ g0 -> f1 ≈ g1 -> f1 ∘ f0 ≈ g1 ∘ g0.
Proof.
  move=> fg0 fg1 a; split=> /=; apply: transitive.
  1,4: apply: (monotone f1).
  - apply: (sprl (fg0 a)).
  - apply: (sprr (fg0 a)).
  - apply: (sprl (fg1 _)).
  - apply: (sprr (fg1 _)).
Qed.

Lemma comp_left [A B C] (f : A →mon B) [g0 g1 : B →mon C]:
  g0 ≈ g1 -> g0 ∘ f ≈ g1 ∘ f.
Proof. move=> g01; apply: comp_monotone_split=> //; PE.refl. Qed.

Lemma comp_right [A B C] [f0 f1 : A →mon B] (g : B →mon C):
  f0 ≈ f1 -> g ∘ f0 ≈ g ∘ f1.
Proof. move=> f01; apply: comp_monotone_split=> //; PE.refl. Qed.

(* TODO : refactor *)
Notation comp_pointwise_subst := comp_monotone_split.

Lemma comp_assoc [A0 A1 A2 A3] (f01 : A0 →mon A1) (f12 : A1 →mon A2) (f23 : A2 →mon A3):
    f23 ∘ (f12 ∘ f01) ≈ (f23 ∘ f12) ∘ f01.
Proof. move=> a /=; apply: equiv_refl. Qed.

Lemma comp_left_unit [A0 A1] (f : A0 →mon A1) :
  f ∘ idmon _ ≈ f.
Proof. move=> a /=; apply: equiv_refl. Qed.

Lemma comp_right_unit [A0 A1] (f : A0 →mon A1) :
  idmon _ ∘ f ≈ f.
Proof. move=> a /=; apply: equiv_refl. Qed.

Definition comp_monotone_left_id_ext [A B] (f g : A →mon B) h :
    f ≈ g -> h ≈ idmon B -> f ≈ h ∘ g.
Proof.
  move=> fg heq; PE.trans; first (PE.sym ; apply: comp_right_unit).
  apply: comp_monotone_split; last PE.sym ; assumption.
Qed.

Definition comp_monotone_left_id [A B] (f g : A →mon B) :
    f ≈ g -> f ≈ idmon B ∘ g.
Proof.
  move=> fg; uref comp_monotone_left_id_ext=> //; PE.refl.
Qed.

Definition comp_monotone_right_id_ext [A B] (f g : A →mon B) h :
    f ≈ g -> h ≈ idmon _ -> f ≈ g ∘ h.
Proof.
  move=> fg heq; lhsrev apply: comp_left_unit;
  apply: comp_monotone_split; first PE.sym ; assumption.
Qed.

Definition comp_monotone_right_id [A B] (f g : A →mon B):
    f ≈ g -> f ≈ g ∘ idmon _.
Proof.
  move=> fg; uref comp_monotone_right_id_ext=> //; PE.refl.
Qed.



(** ** Isomorphisms between preorders *)

Record preorder_iso {X Y : Preorder} :=
  mkIso
    { fwd :> X →mon Y
    ; bwd : Y →mon X
    ; fbeq : fwd ∘ bwd ≈ idmon _
    ; bfeq : bwd ∘ fwd ≈ idmon _
    }.

Arguments preorder_iso : clear implicits.

Lemma iso_flip_eq [X X' Y Y' : Preorder]
      (φ : preorder_iso X X') (f : X' →mon Y')
      (ψ : preorder_iso Y Y') (g : X →mon Y) :
  f ∘ φ ≈ ψ ∘ g -> bwd ψ ∘ f ≈ g ∘ bwd φ.
Proof.
  move=> /(comp_right (bwd ψ)) /(comp_left (bwd φ)) h.
  lhs apply: comp_monotone_right_id_ext; [ PE.refl | apply: (fbeq φ)|].
  lhs apply: comp_assoc; lhs apply: h.
  apply: comp_monotone_split; first PE.refl.
  lhs apply: comp_assoc.
  PE.sym ; apply: comp_monotone_left_id_ext; first PE.refl.
  apply: bfeq.
Qed.


(** ** Preorders  Indexed over a preorder *)

Record IPreorder (A : Preorder) :=
  mkIPreorder
    { icarrier :> A -> Preorder
    ; imap : forall {a0 a1}, a0 ≤ a1 -> icarrier a0 →mon icarrier a1
    ; icomp : forall x y z (xy : x ≤ y) (yz : y ≤ z), imap (xy ∙ yz) ≈ imap yz ∘ imap xy
    ; iid : forall x, imap (reflexive x) ≈ idmon (icarrier x) }.

Arguments imap [_] _ [_ _] _.
Arguments icomp [_ _ _ _ _] _ _.
Arguments iid [_ _]  _.

Program Definition iop {A : Preorder} (B : IPreorder A) : IPreorder A :=
  mkIPreorder A (fun a => op (B a)) (fun a0 a1 a01 => op_mon (imap B a01)) _ _.
Next Obligation.
  move=> A B x y z xy yz a /=;move: (@icomp A B x y z xy yz a)=> [? ?] ; by split.
Qed.
Next Obligation.
  move=> A B x a /=;move: (@iid A B x a)=> [? ?] ; by split.
Qed.

Program Definition pb {A0 A1 : Preorder}  (A01 : A1 →mon A0) (B0 : IPreorder A0) : IPreorder A1 :=
  mkIPreorder A1 (B0 \o A01) (fun _ _ a01 => imap B0 (monotone A01 a01)) _ _.
Next Obligation.
  move=> ? ? A01 ? ? ? ? xy yz /=.
  change (monotone _ (_ ∙ _)) with (monotone A01 xy ∙ monotone A01 yz).
  apply: icomp.
Qed.
Next Obligation.
  move=> ? ? A01 ? x /=.
  change (monotone _ _) with (reflexive (A01 x)).
  apply: iid.
Qed.

Record natTrans {A : Preorder} (B0 B1 : IPreorder A) :=
  mkNatTrans
    { ntmap :> forall a, B0 a →mon B1 a
    ; ntnatural : forall a0 a1 (a01 : a0 ≤ a1),
          ntmap a1 ∘ imap B0 a01 ≈ imap B1 a01 ∘ ntmap a0 }.

Arguments ntnatural [_ _ _] _ [_ _] _.


(** ** TO FILL IN AND CLEAN UP **)


Program Definition comp_natTrans [A] [B0 B1 B2 : IPreorder A]
        (b01 : natTrans B0 B1) (b12 : natTrans B1 B2): natTrans B0 B2 :=
  mkNatTrans _ _ _ (fun a=> b12 a ∘ b01 a) _.
Next Obligation.
  move=> A B0 B1 B2 b01 b12 a0 a1 a01 /=.
  lhsrev apply: comp_assoc;
  lhs (apply: comp_monotone_split; [uref ntnatural| PE.refl]);
  lhs apply: comp_assoc;
  lhs (apply: comp_monotone_split; [PE.refl | uref ntnatural]);
  PE.refl.
Qed.


Program Definition id_natTrans [A] (B : IPreorder A) : natTrans B B :=
  mkNatTrans _ _ _ (fun a => idmon (B a)) _.
Next Obligation.
  move=> A B a0 a1 a01 x /=; apply: equiv_refl.
Qed.

Definition pb_natTrans [A0 A1] (A01 : A0 →mon A1) [B B' : IPreorder A1]:
    natTrans B B' -> natTrans (pb A01 B) (pb A01 B').
Proof.
  move=> BB' ;ueconstr=> a /=.
  apply: BB'.
  move=> ? ? ?; uref ntnatural.
Defined.

Definition comp_natTrans_pb [A0 A1 A2] [A01 : A1 →mon A0] [A12 : A2 →mon A1]
           [B0 B1 B2]
           (B01 : natTrans (pb A01 B0) B1)
           (B12 : natTrans (pb A12 B1) B2):
  natTrans (pb (A01 ∘ A12) B0) B2 :=
  comp_natTrans (pb_natTrans A12 B01) B12.


Definition natTrans_equiv [A] [B B' : IPreorder A] (f g : natTrans B B'): SProp :=
  forall (a : A), f a ≈ g a.

Definition natTrans_equiv_refl [A] [B B' : IPreorder A] (f : natTrans B B') :
  natTrans_equiv f f.
Proof. move=> ? ; apply: pointwise_equiv_refl. Qed.

Program Definition pb_eq [A0 A1] [A01 A01' : A1 →mon A0]
           (Aeq : A01 ≈ A01') (B : IPreorder A0):
  natTrans (pb A01 B) (pb A01' B) :=
  mkNatTrans _ _ _ (fun a => imap B (sprl (Aeq a))) _.
Next Obligation.
  move=> A0 A1 A01 A01' Aeq B a0 a1 a01 /=.
  uref pointwise_equiv_trans. 3: uref icomp.
  uref pointwise_equiv_trans. 2: (apply: pointwise_equiv_sym ; uref icomp).
  apply pointwise_equiv_refl.
Qed.

Definition natTrans_id_skew [A] (B : IPreorder A) : natTrans (pb (idmon A) B) B.
Proof.
  ueconstr.
  move=> a; exact (id_natTrans B a).
  abstract (move=> * /= ? /=; apply: equiv_refl).
Defined.


(* not yet used *)
(* Lemma natTrans_pb_dom_pointwise_equiv [A0 A1] (A01 A01' : A1 →mon A0) (B0 : IPreorder A0) (B1 : IPreorder A1) : A01 ≈ A01' -> *)
(*   natTrans (pb A01 B0) B1 -> natTrans (pb A01' B0) B1. *)
(* Proof. admit. Admitted. *)


