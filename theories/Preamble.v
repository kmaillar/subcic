From Coq Require Export ssreflect ssrfun.
From Coq.Program Require Equality.

(* Using this dependency for eq_above... *)
From SPropTools Require Export Logic.

Set Primitive Projections.
Set Universe Polymorphism.
Obligation Tactic := idtac.

(** ** (Negative) Sigma types *)

Record nsigT {A} (P : A -> Type) :=
  dpair {
    dfst : A ;
    dsnd : P dfst
  }.

Notation "{ x : A ⫳ P }" := (nsigT (fun (x:A) => P)) (x at level 99).
Arguments dpair {_} _ _ _.
Arguments dfst {_ _} _.
Arguments dsnd {_ _} _.

Lemma transport_nsigT : forall {A B} (F : B -> A -> Type) {x y} h z,
    eq_rect x (fun x => { b : B ⫳ F b x}) z y h
    = {| dfst := dfst z ; dsnd := eq_rect x (F (dfst z)) (dsnd z) y h |}.
Proof.
  intros.
  pattern y, h. set P := (fun y : A => _).
  refine (match h as h in _ = y return P y h with
          | eq_refl => _ end).
  reflexivity.
Qed.

Notation "p =⟨ e ⟩ q" := (eq_above e p q) (at level 30).

Lemma eq_eq_above_eq_nsigT :
  forall (U:Type) (P:U -> Type) (z1 z2 : { u : U ⫳ P u }),
    forall h : dfst z1 = dfst z2,  dsnd z1 =⟨ h ⟩ dsnd z2 -> z1 = z2.
Proof.
  intros U P [w1 p1] [w2 p2]; simpl; intros h <-.
  dependent inversion h.
  reflexivity.
Qed.

Lemma eq_above_nsigT_hprop {A B} (f : A -> B -> Type)
      (hprop_f : forall x y (p q : f x y), p = q)
      (G := fun y => {x : A ⫳ f x y})
      {y1 y2 : B} {h : y1 = y2}
      {z1 : G y1} {z2 : G y2} :
  dfst z1 = dfst z2 -> z1 =⟨ h ⟩ z2.
Proof.
  intro Hz.
  unfold eq_above.
  unfold G in *.
  rewrite (transport_nsigT _ h z1).
  eapply (eq_eq_above_eq_nsigT _ _ _ _).
  Unshelve. 2: simpl ; assumption.
  apply hprop_f.
Qed.


(** ** Subset types with proof irrelevant predicates *)

Record subset (X : Type) (P : X -> SProp) :=
  mkSubset { wit : X ; pf : P wit }.

Arguments mkSubset [_] _ _ _.
Arguments wit [_ _] _.
Arguments pf [_ _] _.

(** ** Pervasives tactics *)

Ltac uref t :=
  unshelve first
    [ refine t
    | refine (t _)
    | refine (t _ _)
    | refine (t _ _ _)
    | refine (t _ _ _ _)
    | refine (t _ _ _ _ _)
    | refine (t _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    ].

Ltac sref t :=
  first
    [ simple refine t
    | simple refine (t _)
    | simple refine (t _ _)
    | simple refine (t _ _ _)
    | simple refine (t _ _ _ _)
    | simple refine (t _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    | simple refine (t _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _)
    ].

Ltac ueconstr := unshelve econstructor.

Notation triv_pred := ltac:(repeat intro; exact unit).
Ltac discharge_triv_pred := move=> /= *; exact tt.

